﻿using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.API
{
    public class IsReadyEventArgs : IIsReadyEventArgs
    {
        public Farmer Who { get; }
        public IsReadyEventArgs(Farmer who)
        {
            this.Who = who;
        }
    }
}
