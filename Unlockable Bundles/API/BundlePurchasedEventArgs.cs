﻿using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.API
{
    public class BundlePurchasedEventArgs : IBundlePurchasedEventArgs
    {
        public Farmer Who { get; }
        public string Location { get; }
        public string LocationOrUnique { get; }
        public IBundle Bundle { get; }
        public bool IsBuyer { get; }

        public BundlePurchasedEventArgs(Farmer who, string location, string locationOrUnique, string bundleKey, bool isBuyer)
        {
            this.Who = who;
            this.Location = location;
            this.LocationOrUnique = locationOrUnique;
            this.Bundle = UnlockableBundlesAPI.getBundleForAPI(bundleKey, locationOrUnique);
            this.IsBuyer = isBuyer;
        }
    }
}
