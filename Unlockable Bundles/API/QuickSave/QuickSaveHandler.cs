﻿using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.Lib;
using static Unlockable_Bundles.ModEntry;

namespace Unlockable_Bundles.API.QuickSave
{
    internal class QuickSaveHandler
    {
        public static void Initialize()
        {
            if (Helper.ModRegistry.IsLoaded("DLX.QuickSave"))
                Helper.Events.GameLoop.GameLaunched += GameLaunched;
        }

        private static void GameLaunched(object sender, StardewModdingAPI.Events.GameLaunchedEventArgs e)
        {
            var api = Helper.ModRegistry.GetApi<IQuickSaveAPI>("DLX.QuickSave");
            api.SavingEvent += Api_SavingEvent;
            api.SavedEvent += Api_SavedEvent;
            api.LoadingEvent += Api_LoadingEvent;
        }

        private static void Api_LoadingEvent(object sender, ILoadingEventArgs e)
        {
            var xTileContentCache = Helper.Reflection.GetField<Dictionary<string, object>>(Game1.game1.xTileContent, "loadedAssets").GetValue();
            foreach (var unlockable in MapPatches.AppliedUnlockables) {
                var loc = Unlockable.GetGameLocation(unlockable.Location, unlockable.LocationUnique);
                if (loc?.mapPath.Value is not null) {

                    var key = loc.mapPath.Value.Replace('\\', '/');
                    xTileContentCache.Remove(key);
                }
            }

            var shops = ShopObject.getAll();
            foreach (var shop in shops)
                shop.UnsubscribeFromAllEvents();

            ShopPlacement.CleanupDay();
        }

        private static void Api_SavingEvent(object sender, ISavingEventArgs e)
        {
            var shops = ShopObject.getAll();
            foreach (var shop in shops)
                shop.UnsubscribeFromAllEvents();

            SaveDataEvents.DayEnding(null, null);
            ShopPlacement.CleanupDay();
        }

        private static void Api_SavedEvent(object sender, ISavedEventArgs e)
        {
            ShopPlacement.DayStarted(null, null);
        }
    }
}
