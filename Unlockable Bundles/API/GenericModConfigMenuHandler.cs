﻿using GenericModConfigMenu;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Unlockable_Bundles.ModEntry;


namespace Unlockable_Bundles.API
{
    public class GenericModConfigMenuHandler
    {

        public static void Initialize()
        {
            Helper.Events.GameLoop.GameLaunched += gameLaunched;
        }

        private static void gameLaunched(object sender, GameLaunchedEventArgs e)
        {
            var configMenu = Helper.ModRegistry.GetApi<IGenericModConfigMenuApi>("spacechase0.GenericModConfigMenu");
            if (configMenu is null)
                return;

            configMenu.Register(
                mod: ModManifest,
                reset: () => Config = new ModConfig(),
                save: () => {
                    Helper.WriteConfig(Config);
                    DebugLogLevel = Config.DebugLogging ? LogLevel.Debug : LogLevel.Trace;
                }
            );

            configMenu.AddBoolOption(
                mod: ModManifest,
                name: () => "Debug Logging",
                getValue: () => Config.DebugLogging,
                setValue: value => Config.DebugLogging = value,
                tooltip: () => "Will log mostly in LogLevel.Debug instead of LogLevel.Trace"
             );

            configMenu.AddBoolOption(
                mod: ModManifest,
                name: () => "Show ItemTooltip",
                getValue: () => Config.ShowItemTooltip,
                setValue: value => Config.ShowItemTooltip = value,
                tooltip: () => "Whether the item tooltip should show when it can be donated"
             );

            configMenu.AddSectionTitle(ModManifest, () => "Dialogue Bundle Options");

            configMenu.AddNumberOption(
                mod: ModManifest,
                name: () => "Scroll Delay",
                getValue: () => Config.ScrollDelay,
                setValue: value => Config.ScrollDelay = value,
                interval: 1,
                min: 5,
                max: 60
            );

            configMenu.AddNumberOption(
                mod: ModManifest,
                name: () => "Max Characters",
                getValue: () => Config.ScrollCharacterLength,
                setValue: value => Config.ScrollCharacterLength = value,
                interval: 1,
                min: 5,
                max: 25
            );
        }
    }
}
