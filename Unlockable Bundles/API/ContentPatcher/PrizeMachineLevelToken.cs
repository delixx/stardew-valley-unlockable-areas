﻿using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.Lib;
using Unlockable_Bundles.Lib.PrizeMachine;
using Unlockable_Bundles.Lib.WalletCurrency;

namespace Unlockable_Bundles.API.ContentPatcher
{
    internal class PrizeMachineLevelToken
    {
        public bool RequiresContextUpdate = true;
        public bool RequiresInput()
            => true;

        public bool CanHaveMultipleValues(string input = null)
            => false;

        public bool UpdateContext()
            => BaseToken.UpdateContext(ref RequiresContextUpdate);

        public bool IsReady()
            => BaseToken.IsReady();

        public IEnumerable<string> GetValidInputs()
            => BaseToken.GetValidInputsForPrizeMachine();

        /// <summary>Get the current values.</summary>
        /// <param name="input">The input arguments, if applicable.</param>
        public IEnumerable<string> GetValues(string input)
        {
            if (input == null)
                yield break;


            var machine = PrizeMachineHandler.getMachineById(input);
            if (machine is null)
                yield break;

            var value = ModData.GetPrizeMachineSaveData(machine.Id, Game1.player.UniqueMultiplayerID);

            yield return value.PrizeLevel.ToString();
        }
    }
}
