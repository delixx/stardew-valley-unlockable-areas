﻿using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.Lib;
using Unlockable_Bundles.Lib.WalletCurrency;

namespace Unlockable_Bundles.API.ContentPatcher
{
    internal class PersonalCurrencyToken
    {
        public bool RequiresContextUpdate = true;
        public bool RequiresInput()
            => true;

        public bool CanHaveMultipleValues(string input = null)
            => false;

        public bool UpdateContext()
            => BaseToken.UpdateContext(ref RequiresContextUpdate);

        public bool IsReady()
            => BaseToken.IsReady();

        public IEnumerable<string> GetValidInputs()
            => BaseToken.GetValidInputsForWalletCurrency();

        /// <summary>Get the current values.</summary>
        /// <param name="input">The input arguments, if applicable.</param>
        public IEnumerable<string> GetValues(string input)
        {
            if (input == null)
                yield break;

            var currency = WalletCurrencyHandler.GetCurrencyById(input, false);
            if (currency is null)
                yield break;

            var relevantPlayer = WalletCurrencyHandler.GetRelevantPlayer(currency, Game1.player.UniqueMultiplayerID);
            var value = ModData.GetWalletCurrency(currency.Id, relevantPlayer);

            yield return value.ToString();
        }
    }
}
