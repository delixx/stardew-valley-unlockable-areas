﻿using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.Lib;
using Unlockable_Bundles.Lib.PrizeMachine;
using Unlockable_Bundles.Lib.WalletCurrency;
using static Unlockable_Bundles.ModEntry;

namespace Unlockable_Bundles.API.ContentPatcher
{
    internal class BaseToken
    {
        public static bool RequiresContextUpdate { set {
                ContentPatcherHandling.DaysSincePurchaseToken.RequiresContextUpdate = value;
                ContentPatcherHandling.PersonalCurrencyToken.RequiresContextUpdate = value;
                ContentPatcherHandling.PersonalCurrencyTotalToken.RequiresContextUpdate = value;
                ContentPatcherHandling.CollectiveCurrencyToken.RequiresContextUpdate = value;
                ContentPatcherHandling.CollectiveCurrencyTotalToken.RequiresContextUpdate = value;
                ContentPatcherHandling.PrizeMachineLevelToken.RequiresContextUpdate = value;
            } }
        public static bool Ready = false;


        /****
        ** State
        ****/
        /// <summary>Update the values when the context changes.</summary>
        /// <returns>Returns whether the value changed, which may trigger patch updates.</returns>
        public static bool UpdateContext(ref bool requiresContextUpdate)
        {
            if(requiresContextUpdate && Ready) {
                requiresContextUpdate = false;
                return true;
            }
            return false;

            //return requiresContextUpdate;
        }

        /// <summary>Get whether the token is available for use.</summary>
        public static bool IsReady()
        {
            if (SaveGame.loaded is null && !Context.IsWorldReady)
                return false;

            if (ModData.Instance is not null)
                return true;

            if (!Context.IsMainPlayer)
                return Ready;

            if (ModData.Instance is null)
                SaveDataEvents.LoadModData();

            return true;
        }

        public static IEnumerable<string> GetValidInputsForWalletCurrency()
        {
            var currencies = Helper.GameContent.Load<Dictionary<string, WalletCurrencyModel>>(WalletCurrencyHandler.Asset);
            foreach (var currency in currencies)
                yield return currency.Key;
        }
        public static IEnumerable<string> GetValidInputsForPrizeMachine()
        {
            var machines = Helper.GameContent.Load<Dictionary<string, PrizeMachineModel>>(PrizeMachineHandler.Asset);
            foreach (var machine in machines)
                yield return machine.Key;
        }
    }
}
