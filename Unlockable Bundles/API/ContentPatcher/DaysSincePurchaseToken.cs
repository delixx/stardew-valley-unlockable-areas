﻿using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.Lib;

namespace Unlockable_Bundles.API.ContentPatcher
{
    internal class DaysSincePurchaseToken
    {
        public bool RequiresContextUpdate = true;

        public bool RequiresInput()
            => true;

        public bool CanHaveMultipleValues(string input = null)
            => false;

        public bool UpdateContext()
            => BaseToken.UpdateContext(ref RequiresContextUpdate);

        public bool IsReady()
            => BaseToken.IsReady();

        /// <summary>Get the current values.</summary>
        /// <param name="input">The input arguments, if applicable.</param>
        public IEnumerable<string> GetValues(string input)
        {
            if (input == null)
                yield break;

            var days = ModData.GetDaysSincePurchase(input);

            yield return days.ToString();
        }
    }
}
