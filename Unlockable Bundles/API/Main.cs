﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.API.ContentPatcher;
using Unlockable_Bundles.API.QuickSave;

namespace Unlockable_Bundles.API
{
    internal class Main
    {
        public static void Initialize()
        {
            ContentPatcherHandling.Initialize();
            UnlockableBundlesAPI.Initialize();
            GenericModConfigMenuHandler.Initialize();
            QuickSaveHandler.Initialize();
        }
    }
}
