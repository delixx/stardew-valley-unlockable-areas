﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles {
    public class ModConfig
    {
        public int ScrollDelay { get; set; }
        public int ScrollCharacterLength { get; set; }

        public bool DebugLogging { get; set; }
        public bool ShowItemTooltip { get; set; }

        public ModConfig()
        {
            ScrollDelay = 25;
            ScrollCharacterLength = 10;
            DebugLogging = false;
            ShowItemTooltip = true;
        }
    }
}
