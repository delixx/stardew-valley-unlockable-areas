﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.Lib;

namespace Unlockable_Bundles.NetLib
{
    public class ShopObjectTransferModel
    {
        public Vector2 TileLocation;
        public bool WasDiscovered;
        public UnlockableModel Unlockable;
        public List<UnlockableModel> Pages = new();

        public ShopObjectTransferModel() { }
        public ShopObjectTransferModel(ShopObject shop)
        {
            if (shop is null)
                return;

            TileLocation = shop.TileLocation;
            WasDiscovered = shop.WasDiscovered;
            Unlockable = (UnlockableModel)shop.Unlockable;
            foreach (var page in shop._pages)
                Pages.Add((UnlockableModel)page);
        }
    }
}
