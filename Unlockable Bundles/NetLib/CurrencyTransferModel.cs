﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.NetLib
{
    public class CurrencyTransferModel
    {
        public string CurrencyId;
        public long Who;
        public int AddedValue;
    }
}
