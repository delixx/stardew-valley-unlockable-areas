﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.NetLib
{
    internal class BundleDiscoveredTransferModel
    {
        public string Id;
        public string Location;
        public bool Value;
    }
}
