﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Netcode;
using StardewValley;
using StardewValley.Network;

namespace Unlockable_Bundles.NetLib
{
    public class NetStringIntDictionary : NetDictionary<string, int, NetInt, SerializableDictionary<string, int>, NetStringDictionary<int, NetInt>>
    {
        public NetStringIntDictionary() { }
        public NetStringIntDictionary(IEnumerable<KeyValuePair<string, int>> dict)
        {
            CopyFrom(dict);
        }

        protected override int getFieldTargetValue(NetInt field)
        {
            return field.Value;
        }

        protected override int getFieldValue(NetInt field)
        {
            return field.Value;
        }

        protected override string ReadKey(BinaryReader reader)
        {
            return reader.ReadString();
        }

        protected override void setFieldValue(NetInt field, string key, int value)
        {
            field.Value = value;
        }

        protected override void WriteKey(BinaryWriter writer, string key)
        {
            writer.Write(key);
        }


        //We replace values this way to avoid desync caused by creating a new instance and filling it with values
        public void ReplaceValues(IEnumerable<KeyValuePair<string, int>> newValues)
        {
            this.Clear();
            foreach(var kvp in newValues)
                this.Add(kvp.Key, kvp.Value);
        }
    }
}
