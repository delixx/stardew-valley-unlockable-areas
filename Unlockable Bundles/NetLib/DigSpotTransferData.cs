﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.NetLib
{
    internal class DigSpotTransferData
    {
        public long Who;
        public string Location;
        public int X;
        public int Y;
        public bool ResetMailFlag;
    }
}
