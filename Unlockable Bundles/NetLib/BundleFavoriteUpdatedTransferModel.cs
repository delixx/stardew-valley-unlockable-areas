﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.NetLib
{
    internal class BundleFavoriteUpdatedTransferModel
    {
        public long Farmer;
        public bool Favorite;
        public string BundleId;
        public string BundleLocation;
    }
}
