﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib.Enums
{
    public enum ShopType
    {
        Dialogue = 0,
        SpeechBubble = 1,
        ParrotPerch = 2,
        CCBundle = 3,
        AltCCBundle = 4,
        CCMagicBook = 5,
    }
}
