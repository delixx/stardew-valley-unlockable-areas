﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib.Enums
{
    public enum PlacementRequirementType
    {
        BundleCompletion = 0,
        TriggerAction = 1,
        TimeReached = 2,
    }
}
