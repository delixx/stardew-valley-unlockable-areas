﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib.Enums
{
    public enum BundleIconType
    {
        //Crafts Room
        Spring_Foraging,
        Summer_Foraging,
        Fall_Foraging,
        Winter_Foraging,
        Exotic_Foraging,
        Construction,

        //Pantry
        Spring_Crops,
        Summer_Crops,
        Fall_Crops,
        Quality_Crops,
        Animal,
        Artisan,

        //Fish Tank
        River_Fish,
        Lake_Fish,
        Ocean_Fish,
        Night_Fishing,
        Crab_Pot,
        Specialty_Fish,

        //Boiler
        Blacksmith,
        Geologist,
        Adventurer,

        //Bulletin Board
        Chef,
        Dye,
        Field_Research,
        Fodder,
        Enchanter,

        //Vault
        Small_Money,
        Medium_Money,
        Large_Money,
        Extra_Large_Money,

        //JojaMart,
        Junimo,

        //Remixed
        Sticky,
        Wild_Medicine,
        Rare_Crops,
        Fish_Farmer,
        Garden,
        Brewer,
        Quality_Fish,
        Master_Fisher,
        Treasure_Hunter,
        Children,
        Forager,
        Home_Cook,

        //Unused?
        Volcano,
        Fruit_Tree,
        Spring_Fishing,
        Summer_Fishing,
        Fall_Fishing,
        Winter_Fishing,
    }
}
