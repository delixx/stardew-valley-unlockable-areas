﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewValley;
using StardewModdingAPI;
using static Unlockable_Bundles.ModEntry;
using System.Xml.Serialization;
using System.IO;
using StardewValley.Internal;
using StardewValley.GameData;

namespace Unlockable_Bundles.Lib
{
    public static class UtilityMisc
    {
        public static Texture2D TinyLetters;
        public static Texture2D MoneyTexture;

        public static void Initialize()
        {
            TinyLetters = Helper.ModContent.Load<Texture2D>("assets/TinyLetters.png");
        }

        public static Texture2D CropTexture(Texture2D texture)
        {
            int count = texture.Bounds.Width * texture.Bounds.Height;
            Color[] data = new Color[count];
            texture.GetData(0, texture.Bounds, data, 0, count);

            int width = texture.Bounds.Width;
            int height = texture.Bounds.Height;

            // Initialize boundaries with extreme values
            int left = width, right = 0, top = height, bottom = 0;
            bool hasVisiblePixel = false;

            for (int i = 0; i < count; i++) {
                if (data[i].A != 0)
                {
                    hasVisiblePixel = true;

                    int x = i % width;
                    int y = i / width;

                    if (x < left) left = x;
                    if (x > right) right = x;
                    if (y < top) top = y;
                    if (y > bottom) bottom = y;
                }
            }

            if (!hasVisiblePixel)
                return texture;

            // Create a rectangle that bounds the visible area
            Rectangle cropRect = new Rectangle(left, top, right - left + 1, bottom - top + 1);

            return CreateSubTexture(texture, cropRect);
        }

        public static Texture2D CreateSubTexture(Texture2D src, Rectangle rect)
        {
            Texture2D tex = new Texture2D(Game1.graphics.GraphicsDevice, rect.Width, rect.Height);
            int count = rect.Width * rect.Height;
            Color[] data = new Color[count];
            src.GetData(0, rect, data, 0, count);
            tex.SetData(data);
            return tex;
        }

        public static void DrawMoneyKiloFormat(SpriteBatch b, int num, int x, int y, Color color, float scale = 4f)
        {
            if (MoneyTexture == null)
                MoneyTexture = CreateSubTexture(Game1.mouseCursors, new Rectangle(280, 412, 15, 14));

            b.Draw(MoneyTexture, new Vector2(x, y), new Rectangle(0, 0, 15, 14), Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.9f);
            DrawKiloFormat(b, num, x, y, color, scale * 0.75f);
        }

        public static void DrawKiloFormat(SpriteBatch b, int num, int x, int y, Color color, float scale = 3f)
        {
            string text = num switch {
                >= 1000000 => num / 1000000 + "M",
                >= 1000 => num / 1000 + "K",
                _ => num.ToString()
            };

            for (int i = 0; i < text.Length; i++) {
                if ((byte)text[i] < 65)
                    b.Draw(Game1.mouseCursors, new Vector2(x + (21 * scale) - ((text.Length - i) * 5 * scale), y + 15 * scale), new Rectangle(368 + ((byte)text[i] - 48) * 5, 56, 5, 7), color, 0f, Vector2.Zero, scale, SpriteEffects.None, 1f);
                else
                    b.Draw(TinyLetters, new Vector2(x, y) + new Vector2(16f * scale, 15f * scale), text[i] == 'K' ? new Rectangle(0, 0, 5, 7) : new Rectangle(5, 0, 7, 7), color, 0f, Vector2.Zero, scale, SpriteEffects.None, 1f);
            }
        }

        public static string AsHexString(this Color color)
            => $"#{color.R.ToString("X")}{color.G.ToString("X")}{color.B.ToString("X")}{color.A.ToString("X")}";

        public static string SerializeXML<T>(this T toSerialize)
        {
            if (toSerialize is null)
                return "";

            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());
            using (var writer = new StringWriter()) {
                xmlSerializer.Serialize(writer, toSerialize);
                return writer.ToString();
            }
        }

        public static T? DeserializeXML<T>(this string toDeserialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            using (var reader = new StringReader(toDeserialize)) {
                return (T?)xmlSerializer.Deserialize(reader);
            }
        }

        public static List<Item> GetSpawnFieldItems(List<GenericSpawnItemDataWithCondition> list)
        {
            var ret = new List<Item>();
            ItemQueryContext itemQueryContext = new();
            foreach (GenericSpawnItemDataWithCondition entry in list) {
                if (!GameStateQuery.CheckConditions(entry.Condition))
                    continue;

                var items = ItemQueryResolver.TryResolve(entry, itemQueryContext, logError: (query, message) => Monitor.Log($"Failed parsing item query '{query}': {message}", LogLevel.Warn));
                foreach (var queryResult in items)
                    if (queryResult.Item is Item item)
                        ret.Add(item);
            }
            return ret;
        }
    }
}
