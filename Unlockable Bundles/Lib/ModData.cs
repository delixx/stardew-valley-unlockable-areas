﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Netcode;
using StardewValley;
using Newtonsoft.Json.Linq;
using StardewValley.Network;
using Unlockable_Bundles.NetLib;
using Newtonsoft.Json;
using StardewModdingAPI;
using static Unlockable_Bundles.ModEntry;
using Unlockable_Bundles.Lib.WalletCurrency;
using Unlockable_Bundles.Lib.PrizeMachine;
using Unlockable_Bundles.Lib.AdvancedPricing;
using SObject = StardewValley.Object;

namespace Unlockable_Bundles.Lib
{
    public sealed class ModData
    {
        public static ModData Instance = null;

        //Dic<UnlockableKey, Dic<locationUnique, SaveData>>
        public Dictionary<string, Dictionary<string, UnlockableSaveData>> UnlockableSaveData { get; set; } = new();
        //Dic<Location:x,y , List<Farmers>>
        public Dictionary<string, List<long>> FoundUniqueDigSpots { get; set; } = new();

        //Bundle Keys of SpecialPlacementRequirements
        public List<string> SPRTriggerActionKeys { get; set; } = new();
        //Dic< CurrencyId, Dic< playerId, value >>
        //if it's shared the main players will be used
        //If no entry in the second dictionary exists I'll treat it as the player not having obtained this currency yet
        public Dictionary<string, Dictionary<long, int>> WalletCurrencyData { get; set; } = new();
        public Dictionary<string, Dictionary<long, long>> WalletCurrencyTotal { get; set; } = new();

        //Dic<  PrizeMachineId, Dic< playerId< value >>
        public Dictionary<string, Dictionary<long, PrizeMachineSaveData>> PrizeMachineData { get; set; } = new();
        public static bool IsUnlockablePurchased(string key, string location)
        {
            EnsureExist(key, location);

            return Instance.UnlockableSaveData[key][location].Purchased;

        }

        private static void EnsureExist(string key, string location)
        {
            if (!Instance.UnlockableSaveData.ContainsKey(key))
                Instance.UnlockableSaveData[key] = new Dictionary<string, UnlockableSaveData>();

            if (!Instance.UnlockableSaveData[key].ContainsKey(location))
                Instance.UnlockableSaveData[key][location] = new UnlockableSaveData();
        }

        public static void SetPurchased(string key, string location, bool value = true)
        {
            EnsureExist(key, location);

            Instance.UnlockableSaveData[key][location].Purchased = value;
            Instance.UnlockableSaveData[key][location].DayPurchased = Game1.Date.TotalDays;
            API.UnlockableBundlesAPI.ClearCache();
        }

        public static void SetPartiallyPurchased(string key, string location, string requirement, int value, int index = -1)
        {
            EnsureExist(key, location);

            Instance.UnlockableSaveData[key][location].AlreadyPaid.TryAdd(requirement, value); //TryAdd because this code can run twice in splitscreen
            if (index != -1)
                Instance.UnlockableSaveData[key][location].AlreadyPaidIndex.TryAdd(requirement, index);
            API.UnlockableBundlesAPI.ClearCache();
        }

        public static void SetDiscovered(string key, string location, bool value = true)
        {
            EnsureExist(key, location);

            Instance.UnlockableSaveData[key][location].Discovered = value;
            API.UnlockableBundlesAPI.ClearCache();
        }

        public static bool GetDiscovered(string key, string location, bool value = true)
        {
            EnsureExist(key, location);

            return Instance.UnlockableSaveData[key][location].Discovered;
        }

        //Returns the most recent purchase day of an unlockable or -1
        public static int GetDaysSincePurchase(string key)
        {
            if (Instance == null)
                return -1;

            if (!Instance.UnlockableSaveData.ContainsKey(key))
                return -1;

            var entries = Instance.UnlockableSaveData[key].Where(e => e.Value.Purchased);

            if (!entries.Any())
                return -1;

            return Game1.Date.TotalDays - entries.OrderBy(e => e.Value.DayPurchased).First().Value.DayPurchased;
        }

        public static UnlockableSaveData GetUnlockableSaveData(string key, string location)
        {
            EnsureExist(key, location);

            return Instance.UnlockableSaveData[key][location];
        }

        public static void CheckLegacySaveData()
        {
            const string customDataKey = "smapi/mod-data/delixx.unlockable_areas/main";

            if (!Game1.CustomData.ContainsKey(customDataKey))
                return;

            var legacy = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, Dictionary<string, bool>>>>(Game1.CustomData[customDataKey]);

            Game1.CustomData.Remove(customDataKey);

            if (!legacy.ContainsKey("UnlockablePurchased"))
                return;

            foreach (var key in legacy["UnlockablePurchased"])
                foreach (var location in key.Value)
                    SetPurchased(key.Key, location.Key, location.Value);
        }

        public static void ApplySaveData(Unlockable unlockable, UnlockableModel model)
        {
            EnsureExist(unlockable.ID, unlockable.LocationUnique);
            var savedata = Instance.UnlockableSaveData[unlockable.ID][unlockable.LocationUnique];

            if (savedata.Purchased) //We currently don't need to apply savedata for purchased unlockables, since all we do with them is apply map patches
                return;

            ProcessPrice(unlockable, model, savedata);
            EnsureBundleCanBeCompleted(unlockable);

            unlockable._alreadyPaid.ReplaceValues(savedata.AlreadyPaid);
            unlockable._alreadyPaidIndex.ReplaceValues(savedata.AlreadyPaidIndex);
        }

        private static void EnsureBundleCanBeCompleted(Unlockable unlockable)
        {
            //failsafe when for whatever reason there's fewer items than have to be submitted
            var count = unlockable._price.Count();
            if (unlockable.BundleSlots > count)
                unlockable.BundleSlots = count;
            else if (unlockable.BundleSlots <= 0)
                unlockable.BundleSlots = count;
        }

        private static void ProcessPrice(Unlockable unlockable, UnlockableModel model, UnlockableSaveData savedata)
        {
            if (savedata.Price.Any()) {
                ApplyPriceMigration(unlockable, model, savedata);
                unlockable._price.ReplaceValues(savedata.Price);

            } else {
                ApplyPriceSpawnFieldItems(unlockable, model);
                ApplyRandomPriceEntries(unlockable);
                savedata.Price = unlockable._price.Pairs.ToDictionary(x => x.Key, y => y.Value);

            }
        }

        private static void ApplyPriceSpawnFieldItems(Unlockable unlockable, UnlockableModel model)
        {
            if (model.PriceSpawnFields.Count > 0) {
                Monitor.Log($"Generating bundle price spawn field items for {unlockable.ID} : {unlockable.LocationUnique}", DebugLogLevel);
                var items = UtilityMisc.GetSpawnFieldItems(model.PriceSpawnFields);

                foreach (var item in items) {
                    string newId = item.QualifiedItemId;

                    if (item is SObject obj && obj.preservedParentSheetIndex.Value is not null) {
                        SObject.PreserveType? preserveType = obj.ItemId switch {
                            "348" => SObject.PreserveType.Wine,
                            "344" => SObject.PreserveType.Jelly,
                            "342" => SObject.PreserveType.Pickle,
                            "350" => SObject.PreserveType.Juice,
                            "812" => SObject.PreserveType.Roe,
                            "447" => SObject.PreserveType.AgedRoe,
                            "340" => SObject.PreserveType.Honey,
                            "SpecificBait" => SObject.PreserveType.Bait,
                            "DriedFruit" => SObject.PreserveType.DriedFruit,
                            "DriedMushrooms" => SObject.PreserveType.DriedMushroom,
                            "SmokedFish" => SObject.PreserveType.SmokedFish,
                            _ => null
                        };

                        newId = AdvancedPricingItem.FlavoredTypeDefinition
                            + (preserveType is null ? obj.QualifiedItemId : preserveType)
                            + "."
                            + obj.preservedParentSheetIndex.Value;
                    }

                    newId += ":" + item.Quality;

                    //The key has to be unique, but we can have the same entry multiple times by padding spaces to either side since they get trimmed away during item parsing
                    while (unlockable._price.ContainsKey(newId))
                        newId = " " + newId;

                    unlockable._price.Add(newId, item.Stack);
                    Monitor.Log($"Added '{newId}' x {item.Stack}", DebugLogLevel);
                }
            }
        }

        private static void ApplyRandomPriceEntries(Unlockable unlockable)
        {
            if (unlockable.RandomPriceEntries > 0) {
                var randomizedPrice = unlockable._price.Pairs.OrderBy(x => Game1.random.Next()).Take(unlockable.RandomPriceEntries).ToDictionary(x => x.Key, x => x.Value);
                unlockable._price.ReplaceValues(randomizedPrice);
            }
        }

        private static void ApplyPriceMigration(Unlockable unlockable, UnlockableModel model, UnlockableSaveData savedata)
        {
            foreach (var migration in model.PriceMigration) {
                if (!savedata.Price.ContainsKey(migration.Key) && !savedata.AlreadyPaid.ContainsKey(migration.Key) && !savedata.AlreadyPaidIndex.ContainsKey(migration.Key))
                    continue;

                if (migration.Value.Trim().ToLower() == "remove") { //Remove Keyword
                    savedata.Price.Remove(migration.Key);
                    savedata.AlreadyPaid.Remove(migration.Key);
                    savedata.AlreadyPaidIndex.Remove(migration.Key);
                    Monitor.Log($"Removed price savedata in bundle '{unlockable.ID}' for '{migration.Key}'");
                    continue;
                }

                if (migration.Value.Trim().ToLower() == "reroll") { //Reroll
                    var unusedPriceEntries = unlockable._price.Pairs.Where(e => !savedata.Price.ContainsKey(e.Key)).ToDictionary(x => x.Key, x => x.Value);

                    if (unusedPriceEntries.Count == 0) {
                        Monitor.LogOnce($"PriceMigration reroll requested for '{migration.Key}' of bundle '{unlockable.ID}', but there's no unused Price entries left\n"
                            + "Reroll request will be ignored", LogLevel.Warn);
                        continue;
                    }

                    var randomPrice = unusedPriceEntries.ElementAt(Game1.random.Next(unusedPriceEntries.Count));
                    ApplyPriceEntryMigration(unlockable, savedata, migration.Key, randomPrice.Key, randomPrice.Value);
                    continue;
                }

                if (!unlockable._price.TryGetValue(migration.Value, out var newPriceAmount)) {
                    Monitor.LogOnce($"PriceMigration requested from '{migration.Key}' to '{migration.Value}' of bundle '{unlockable.ID}' without a matching Price entry.\n"
                        + "Migration request will be ignored.", LogLevel.Warn);
                    continue;
                }

                ApplyPriceEntryMigration(unlockable, savedata, migration.Key, migration.Value, newPriceAmount);
            }
        }

        private static void ApplyPriceEntryMigration(Unlockable unlockable, UnlockableSaveData savedata, string oldPriceKey, string newPriceKey, int newPriceAmount)
        {
            if (savedata.AlreadyPaid.TryGetValue(oldPriceKey, out var alreadyPaidAmount)) {
                savedata.AlreadyPaid.Remove(oldPriceKey);
                savedata.AlreadyPaid.Add(newPriceKey, alreadyPaidAmount);

                if (savedata.AlreadyPaidIndex.TryGetValue(oldPriceKey, out var alreadyPaidIndex)) {
                    savedata.AlreadyPaidIndex.Remove(oldPriceKey);
                    savedata.AlreadyPaidIndex.Add(newPriceKey, alreadyPaidIndex);
                }
            }

            if (savedata.Price.Count != 0) {
                savedata.Price.Remove(oldPriceKey);
                savedata.Price.Add(newPriceKey, newPriceAmount);
            }

            Monitor.Log($"Migrated Price in bundle '{unlockable.ID}' from '{oldPriceKey}' to '{newPriceKey}':'{newPriceAmount}'");
        }

        private static void AddWalletCurrencyTotal(string currencyId, long who, int value)
        {
            if (value <= 0)
                return;

            if (!Instance.WalletCurrencyTotal.TryGetValue(currencyId, out var data))
                Instance.WalletCurrencyTotal.Add(currencyId, data = new());

            if (!data.ContainsKey(who))
                data.Add(who, 0);

            data[who] += value;
        }
        public static int AddWalletCurrency(string currencyId, long who, int value)
        {
            AddWalletCurrencyTotal(currencyId, who, value);

            if (!Instance.WalletCurrencyData.TryGetValue(currencyId, out var data))
                Instance.WalletCurrencyData.Add(currencyId, data = new());

            if (!data.ContainsKey(who))
                data.Add(who, 0);

            return data[who] = Math.Clamp(data[who] + value, 0, int.MaxValue);
        }

        public static int GetWalletCurrency(string currencyId, long who)
        {
            if (!Instance.WalletCurrencyData.TryGetValue(currencyId, out var data))
                return 0;

            if (!data.ContainsKey(who))
                return 0;

            return data[who];
        }

        public static long GetWalletCurrencyTotal(string currencyId, long who)
        {
            if (!Instance.WalletCurrencyTotal.TryGetValue(currencyId, out var data))
                return 0;

            if (!data.ContainsKey(who))
                return 0;

            return data[who];
        }

        public static long GetCollectiveWalletCurrency(string currencyId)
        {
            if (!Instance.WalletCurrencyData.TryGetValue(currencyId, out var data))
                return 0;

            long value = 0;
            foreach (var e in data)
                value += e.Value;

            return value;
        }

        public static long GetCollectiveWalletCurrencyTotal(string currencyId)
        {
            if (!Instance.WalletCurrencyTotal.TryGetValue(currencyId, out var data))
                return 0;

            long value = 0;
            foreach (var e in data)
                value += e.Value;

            return value;
        }

        public static bool WalletCurrencyDiscovered(string currencyId, long who)
        {
            if (Instance?.WalletCurrencyData?.TryGetValue(currencyId, out var data) == true)
                return data.ContainsKey(who);

            return false;
        }

        public static PrizeMachineSaveData GetPrizeMachineSaveData(string priceMachineId, long who)
        {
            if (!Instance.PrizeMachineData.TryGetValue(priceMachineId, out var playerData)) {
                playerData = new Dictionary<long, PrizeMachineSaveData>();
                Instance.PrizeMachineData[priceMachineId] = playerData;
            }

            if (!playerData.TryGetValue(who, out var data)) {
                data = new PrizeMachineSaveData();
                playerData[who] = data;
            }

            return data;
        }
    }
}
