﻿using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Unlockable_Bundles.Lib
{
    //We need this because parsing Item directly will fail at converting inherited types like Object to Item during deserialization
    [XmlRoot("item")]
    public struct ParsableItem
    {
        public Item? Item;

        public static implicit operator Item?(ParsableItem parsable)
            => parsable.Item;

        public static explicit operator ParsableItem(Item item)
            => new ParsableItem() { Item = item };
    }
}
