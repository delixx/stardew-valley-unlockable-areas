﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib.MapFeatures
{
    internal class Main
    {
        public static void Initialize()
        {
            UB_NoGrass.Initialize();
            DigSpot.Initialize();
        }
    }
}
