﻿using HarmonyLib;
using Microsoft.Xna.Framework.Graphics;
using StardewModdingAPI;
using StardewModdingAPI.Utilities;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewValley.Menus;
using static Unlockable_Bundles.ModEntry;
using Unlockable_Bundles.Lib.ShopTypes;
using Microsoft.Xna.Framework;
using Unlockable_Bundles.NetLib;

//Appends a small info to the item tooltip when a item is donatable
namespace Unlockable_Bundles.Lib
{
    public class DonatableItemTooltip
    {
        private static PerScreen<List<ShopObject>> Bundles = new();
        public static PerScreen<Item> LastHoverItem = new();
        public static PerScreen<ShopObject> LastHoverResult = new();
        public static PerScreen<bool> WaitingOnBundleRefresh = new();

        public static Texture2D BookIcon;

        public static void Initialize()
        {
            var harmony = new Harmony(ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.Method(typeof(Item), nameof(Item.drawTooltip)),
                postfix: new HarmonyMethod(typeof(DonatableItemTooltip), nameof(DonatableItemTooltip.DrawTooltip_Postfix))
            );

            harmony.Patch(
                original: AccessTools.Method(typeof(Item), nameof(Item.getExtraSpaceNeededForTooltipSpecialIcons)),
                postfix: new HarmonyMethod(typeof(DonatableItemTooltip), nameof(DonatableItemTooltip.getExtraSpaceNeededForTooltipSpecialIcons_Postfix))
            );

            Helper.Events.Display.MenuChanged += MenuChanged;

            BookIcon = Helper.GameContent.Load<Texture2D>("UnlockableBundles/UI/BundleBook");
        }

        private static void MenuChanged(object sender, StardewModdingAPI.Events.MenuChangedEventArgs e)
        {
            if (e.OldMenu is GameMenu or ItemGrabMenu && e.NewMenu is not GameMenu and not ItemGrabMenu) {
                Bundles.Value = null;
                WaitingOnBundleRefresh.Value = false;
                LastHoverItem.Value = null;
                LastHoverResult.Value = null;
                return;

            } else if (e.NewMenu is not GameMenu and not ItemGrabMenu)
                return;

            Bundles.Value = ShopObject.getAll();

            if (!Context.IsMainPlayer) {
                var existing = new List<KeyValuePair<string, string>>();
                foreach (var bundle in Bundles.Value)
                    existing.Add(new(bundle.Unlockable.ID, bundle.Unlockable.LocationUnique));

                WaitingOnBundleRefresh.Value = true;
                Helper.Multiplayer.SendMessage(existing, "RequestBundleRefresh", modIDs: new[] { ModManifest.UniqueID }, playerIDs: new[] { Game1.MasterPlayer.UniqueMultiplayerID });
            }
                
        }

        public static void AppendMissingBundles(List<ShopObjectTransferModel> transferModels)
        {
            if (!WaitingOnBundleRefresh.Value)
                return;

            foreach (var model in transferModels)
                Bundles.Value.Add(new ShopObject(model, false));

            WaitingOnBundleRefresh.Value = false;
            LastHoverItem.Value = null;
        }

        public static void DrawTooltip_Postfix(Item __instance, SpriteBatch spriteBatch, ref int x, ref int y, SpriteFont font, float alpha, StringBuilder overrideText)
        {
            DrawDonatableOverlay(__instance, spriteBatch, ref x, ref y, font, alpha, overrideText);
        }

        public static void DrawDonatableOverlay(Item item, SpriteBatch b, ref int x, ref int y, SpriteFont font, float alpha, StringBuilder overrideText)
        {
            if (!Config.ShowItemTooltip)
                return;

            if (Game1.activeClickableMenu is not GameMenu and not ItemGrabMenu)
                return;

            if (Bundles.Value is null)
                return;

            if (LastHoverItem.Value is null || LastHoverItem.Value != item)
                LastHoverResult.Value = Bundles.Value.Find(
                    e => e.WasDiscovered
                    && ( 
                        Inventory.IsValidItemForThisBundle(e.Unlockable, item) 
                        || e._pages.Any(page => Inventory.IsValidItemForThisBundle(page, item))
                       )
                );
            LastHoverItem.Value = item;

            if (LastHoverResult.Value is null)
                return;

            var text = LastHoverResult.Value.Unlockable.GetDisplayName();
            Utility.drawWithShadow(b, BookIcon, new Vector2(x + 10, y), new Rectangle(0, 0, BookIcon.Width, BookIcon.Height), Color.White, 0f, Vector2.Zero, 1f, flipped: false, 1f);
            Utility.drawTextWithShadow(b, text, font, new Vector2(x + 72, y + 28), new Color(120, 0, 210) * 0.9f * alpha);
            y += BookIcon.Height;
        }

        public static void getExtraSpaceNeededForTooltipSpecialIcons_Postfix(Item __instance, ref Point __result, SpriteFont font, int minWidth, int horizontalBuffer, int startingHeight, StringBuilder descriptionText, string boldTitleText, int moneyAmountToDisplayAtBottom)
        {
            if (!Config.ShowItemTooltip)
                return;

            if (Game1.activeClickableMenu is not GameMenu and not ItemGrabMenu)
                return;

            if (Bundles.Value is null)
                return;

            if (LastHoverResult.Value is null)
                return;

            var text = LastHoverResult.Value.Unlockable.GetDisplayName();
            __result.X = (int)Math.Max(__result.X, minWidth);
            __result.X = (int)Math.Max(__result.X, font.MeasureString(text).X + 72 + 16);

            __result.Y = (int)Math.Max(startingHeight, __result.Y);
            __result.Y += BookIcon.Height; 
        }
    }
}
