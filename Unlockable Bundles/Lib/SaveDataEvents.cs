﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewValley;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using static Unlockable_Bundles.ModEntry;


namespace Unlockable_Bundles.Lib
{
    public class SaveDataEvents
    {
        const string SaveKey = "main";

        public static void Initialize()
        {
            Helper.Events.GameLoop.DayEnding += DayEnding;
        }

        public static void LoadModData()
        {
            ModData.Instance = Helper.Data.ReadSaveData<ModData>(SaveKey) ?? new ModData();
            ModData.CheckLegacySaveData();
        }

        //Solid Foundations has [EventPriority(EventPriority.High + 1)], but we want to run before it
        [EventPriority(EventPriority.High + 2)]
        public static void DayEnding(object sender, DayEndingEventArgs e)
        {
            if (!Context.IsMainPlayer)
                return;

            clearUnlockableShops();

            Helper.Data.WriteSaveData(SaveKey, ModData.Instance);
        }

        //Unlockable ShopObjects will be re-added every DayStart.
        //We delete them during Saving to keep the savefiles clean from custom objects
        private static void clearUnlockableShops()
        {
            foreach (var loc in Game1.locations)
                foreach (var building in loc.buildings.Where(el => el.isUnderConstruction() && el.indoors.Value is not null))
                    if (!ShopPlacement.ModifiedLocations.Contains(building.indoors.Value))
                        ShopPlacement.ModifiedLocations.Add(building.indoors.Value);


            foreach (var modifiedLocation in ShopPlacement.ModifiedLocations) {
                var loc = Game1.getLocationFromName(modifiedLocation.NameOrUniqueName, modifiedLocation.isStructure.Value);

                if (loc is null)
                    continue;

                foreach (ShopObject obj in loc.Objects.Values.Where(el => el is ShopObject)) {
                    Monitor.Log($"Removing Bundle for '{obj.Unlockable.ID}' at '{loc.NameOrUniqueName}':'{obj.TileLocation}'", DebugLogLevel);
                    loc.removeObject(obj.TileLocation, false);
                }
            }

        }
    }
}
