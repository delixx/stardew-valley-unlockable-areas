﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Netcode;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewValley;
using Unlockable_Bundles.Lib.Enums;
using static StardewValley.BellsAndWhistles.ParrotUpgradePerch;
using StardewValley.BellsAndWhistles;
using StardewValley.Menus;
using Unlockable_Bundles.Lib.AdvancedPricing;
using static Unlockable_Bundles.ModEntry;
using Unlockable_Bundles.Lib.WalletCurrency;

namespace Unlockable_Bundles.Lib.ShopTypes
{
    public class SpeechBubble : INetObject<NetFields>
    {
        public NetFields NetFields { get; } = new NetFields("DLX.Bundles/SpeechBubble");

        private NetRef<ParrotUpgradePerch> _parrotPerch = new NetRef<ParrotUpgradePerch>();
        public NetEnum<UpgradeState> CurrentState = new NetEnum<UpgradeState>(UpgradeState.Idle);

        public ShopObject Shop;
        public Unlockable Unlockable { get => Shop.Unlockable; }

        public bool IsPlayerNearby;

        public float StateTimer;
        public float ShakeTime;
        public float CostShakeTime;
        public float SquawkTime;
        public float TimeUntilChomp;
        public float TimeUntilSqwawk;
        public float NextParrotSpawn;

        public ParrotUpgradePerch ParrotPerch { get => _parrotPerch.Value; set => _parrotPerch.Value = value; }
        public List<Parrot> Parrots = new List<Parrot>();
        public bool ParrotPresent = true;
        public const int PARROT_COUNT = 24;

        public NetEvent0 AnimationEvent = new NetEvent0();
        public NetEvent0 UpgradeCompleteEvent = new NetEvent0();
        public bool WaitingForProcessShopEvent = false;

        public KeyValuePair<string, int> NextRequirement;
        public string NextId;
        public int NextQuality;
        public Item NextItem;

        public static void Initialize()
        {
        }
        private void AddNetFieldsAndEvents()
        {
            NetFields.SetOwner(this)
                .AddField(CurrentState,         "CurrentState")
                .AddField(AnimationEvent,       "AnimationEvent")
                .AddField(UpgradeCompleteEvent, "UpgradeCompleteEvent")
                .AddField(_parrotPerch,         "_parrotPerch");

            Helper.Events.GameLoop.UpdateTicked += UpdateTicked;

            AnimationEvent.onEvent += PerformAnimation;
            UpgradeCompleteEvent.onEvent += PerformCompleteAnimation;
        }

        private void UpdateTicked(object sender, UpdateTickedEventArgs e)
        {
            if (Shop != null)
                UpdateEvenIfFarmerIsntHere(Game1.currentGameTime);
        }

        public SpeechBubble() => AddNetFieldsAndEvents();

        public SpeechBubble(ShopObject shop)
        {
            Shop = shop;

            AddNetFieldsAndEvents();
            NetFields.Parent = shop.NetFields;

            if (shop.ShopType == ShopType.ParrotPerch)
                ParrotPerch = new ParrotUpgradePerch(shop.Unlockable.GetGameLocation(), shop.TileLocation.ToPoint(), shop.Unlockable.ParrotTarget, 1, null, null);
                
            AssignNextItem();
        }

        public void UnsubscribeFromAllEvents()
        { //If events aren't unsubscribed this object and associates will probably not be properly garbage collected. This is to prevent a expected memory leak
            Helper.Events.GameLoop.UpdateTicked -= UpdateTicked;
        }

        public void AssignNextItem()
        {
            foreach (var req in Unlockable._price.Pairs)
                if (!Unlockable._alreadyPaid.ContainsKey(req.Key)) {
                    NextRequirement = req;
                    NextId = Unlockable.GetFirstIDFromReqKey(req.Key);
                    NextQuality = Unlockable.GetFirstQualityFromReqKey(req.Key);
                    NextItem = NextId == "money" ? null : Unlockable.ParseItem(NextId, req.Value, NextQuality);
                    return;
                }

            ModData.SetPurchased(Unlockable.ID, Unlockable.LocationUnique);
        }

        public bool IsAtTile(int x, int y)
        {
            if (Shop.TileLocation.X == x && Shop.TileLocation.Y == y)
                return CurrentState.Value == UpgradeState.Idle;

            return false;
        }
        public virtual void UpdateEvenIfFarmerIsntHere(GameTime time)
        {
            AnimationEvent.Poll();
            UpgradeCompleteEvent.Poll();

            if (Game1.IsMasterGame)
                UpdateState(time);

            if (WaitingForProcessShopEvent && CurrentState.Value == UpgradeState.Complete) {
                WaitingForProcessShopEvent = false;
                Unlockable.ProcessShopEvent();

                Shop.Mutex.ReleaseLock();
                UpgradeCompleteEvent.Fire();
            }
        }

        private void UpdateState(GameTime time)
        {
            if (StateTimer > 0f)
                StateTimer -= (float)time.ElapsedGameTime.TotalSeconds;

            if (CurrentState.Value == UpgradeState.StartBuilding && StateTimer <= 0f) {
                CurrentState.Value = UpgradeState.Building;
                StateTimer = Unlockable.ShopType == ShopType.ParrotPerch ? 5f : 0f;
            }

            if (CurrentState.Value == UpgradeState.Building && StateTimer <= 0f) {
                if (ParrotPerch != null)
                    ParrotPerch.currentState.Value = UpgradeState.Complete;

                CurrentState.Value = UpgradeState.Complete;
            }
        }

        public void UpdateWhenCurrentLocation(GameTime time, ShopObject shop)
        {
            FixMultiplayerConstructor(shop);
            UpdateTimers(time);
            UpdateParrots(time);
        }

        //Can happen in multiplayer after sleeping that the parameterless bozo constructor gets used
        private void FixMultiplayerConstructor(ShopObject shop)
        {
            if (Shop is not null)
                return;

            Shop = shop;
            AssignNextItem();
        }

        public void UpdateParrots(GameTime time)
        {
            if (Unlockable.ShopType != ShopType.ParrotPerch)
                return;

            if (CurrentState.Value == UpgradeState.Building && Parrots.Count < 24) {
                if (NextParrotSpawn > 0f)
                    NextParrotSpawn -= (float)time.ElapsedGameTime.TotalSeconds;

                if (NextParrotSpawn <= 0f) {
                    NextParrotSpawn = 0.05f;
                    Rectangle spawn_rectangle = Unlockable.ParrotTarget;
                    spawn_rectangle.Inflate(5, 0);
                    Parrots.Add(new Parrot(ParrotPerch, Utility.getRandomPositionInThisRectangle(spawn_rectangle, Game1.random), Parrots.Count % 10 == 0));
                }
            }
            for (int i = 0; i < Parrots.Count; i++)
                if (Parrots[i].Update(time))
                    Parrots.RemoveAt(i--);
                else if (Parrots[i].isPerchedParrot)
                    //The game resets the birdType to 0 at every update for no reason
                    Helper.Reflection.GetField<int>(Parrots[i], "birdType").SetValue(Unlockable.ParrotIndex);
        }

        public void UpdateTimers(GameTime time)
        {
            if (ShakeTime > 0f)
                ShakeTime -= (float)time.ElapsedGameTime.TotalSeconds;

            if (CostShakeTime > 0f)
                CostShakeTime -= (float)time.ElapsedGameTime.TotalSeconds;

            if (SquawkTime > 0f)
                SquawkTime -= (float)time.ElapsedGameTime.TotalSeconds;

            if (TimeUntilSqwawk > 0f) {
                TimeUntilSqwawk -= (float)time.ElapsedGameTime.TotalSeconds;
                UpdateSquawk();
            }

            if (TimeUntilChomp > 0f) {
                TimeUntilChomp -= (float)time.ElapsedGameTime.TotalSeconds;
                UpdateChomp();
            }

            TransformParrot();
            DoPlayerNearbySqwawk();
        }

        public void DoPlayerNearbySqwawk()
        {
            bool player_nearby = false;
            if (Math.Abs(Game1.player.TilePoint.X - Shop.TileLocation.X) <= 1 && Math.Abs(Game1.player.TilePoint.Y - Shop.TileLocation.Y) <= 1)
                player_nearby = true;

            if (player_nearby != IsPlayerNearby) {
                IsPlayerNearby = player_nearby;

                if (IsPlayerNearby && !Shop.Mutex.IsLocked() && CurrentState.Value == UpgradeState.Idle) {
                    SquawkTime = 0.5f;

                    if (Unlockable.InteractionSound != "")
                        Game1.playSound(Unlockable.InteractionSound);

                    if (Unlockable.InteractionShake) {
                        ShakeTime = 0.5f;
                        CostShakeTime = 0.5f;
                    }

                    if (NextId == "(O)73")
                        Game1.specialCurrencyDisplay.ShowCurrency("walnuts");
                    else if (NextId == "(O)858")
                        Game1.specialCurrencyDisplay.ShowCurrency("qiGems");
                    else if(WalletCurrencyHandler.GetCurrencyItemMatch(NextId, out var match, out var currency, out var relevantPlayer))
                        WalletCurrencyBillboard.ShowCurrency(currency, relevantPlayer);
                } else {
                    Game1.specialCurrencyDisplay.ShowCurrency(null);
                    WalletCurrencyBillboard.StopForceShow();
                }
            }
        }

        public void TransformParrot()
        {
            if (Unlockable.ShopType != ShopType.ParrotPerch || !ParrotPresent || CurrentState.Value <= UpgradeState.StartBuilding)
                return;

            if (CurrentState.Value == UpgradeState.Building) {
                Parrot flying_parrot = new Parrot(ParrotPerch, Shop.TileLocation);
                flying_parrot.isPerchedParrot = true;
                Helper.Reflection.GetField<int>(flying_parrot, "birdType").SetValue(Unlockable.ParrotIndex);
                Parrots.Add(flying_parrot);
            }
            ParrotPresent = false;
        }

        public void UpdateChomp()
        {
            if (TimeUntilChomp > 0f)
                return;

            Game1.playSound("eat");
            TimeUntilChomp = 0f;

            ShakeTime = 0.25f;
            if (ParrotPerch == null)
                Shop.shakeTimer = 250;

            if (Game1.currentLocation.getTemporarySpriteByID(98765) != null) {
                for (int j = 0; j < 6; j++) {
                    Game1.currentLocation.temporarySprites.Add(new TemporaryAnimatedSprite("LooseSprites\\Cursors2", new Rectangle(9, 252, 3, 3), Game1.currentLocation.getTemporarySpriteByID(98765).position + new Vector2(8f, 8f) * 4f, Game1.random.NextDouble() < 0.5, 0f, Color.White) {
                        motion = new Vector2(Game1.random.Next(-1, 2), -6f),
                        acceleration = new Vector2(0f, 0.25f),
                        rotationChange = (float)Game1.random.Next(-4, 5) * 0.05f,
                        scale = 4f,
                        animationLength = 1,
                        totalNumberOfLoops = 1,
                        interval = 500 + Game1.random.Next(500),
                        layerDepth = 1f,
                        drawAboveAlwaysFront = true
                    });
                }
            }
            Game1.currentLocation.removeTemporarySpritesWithID(98765);
            TimeUntilSqwawk = 1f;
        }

        public void UpdateSquawk()
        {
            if (TimeUntilSqwawk > 0)
                return;

            CurrentState.Value = UpgradeState.Idle;

            var allRequirementsPaid = Unlockable.AllRequirementsPaid();

            if (allRequirementsPaid && Shop.Mutex.IsLockHeld()) {
                Unlockable.ProcessPurchase();
                WaitingForProcessShopEvent = true;
            }

            if (Shop.Mutex.IsLockHeld())
                PrepareExit();

            if (allRequirementsPaid)
                CurrentState.Value = UpgradeState.StartBuilding;

            TimeUntilSqwawk = 0f;
            if (Unlockable.InteractionSound != "")
                Game1.playSound(Unlockable.InteractionSound);

            SquawkTime = 0.5f;
            ShakeTime = 0.5f;
            if (ParrotPerch == null)
                Shop.shakeTimer = 500;
        }

        public void Interact(Farmer who)
        {
            if (Unlockable.AllRequirementsPaid())
                return;

            Game1.player.canMove = false;
            ShowYesNo(who);
        }

        public void PrepareExit()
        {
            Shop.Mutex.ReleaseLock();
            Game1.player.canMove = true;
        }

        public bool TryDonate(Farmer who)
        {
            if (!Inventory.hasEnoughItems(who, NextRequirement)) {
                CostShakeTime = 0.5f;
                SquawkTime = 0.5f;
                ShakeTime = 0.5f;
                if (ParrotPerch == null)
                    Shop.shakeTimer = 500;

                if (Unlockable.InteractionSound != "")
                    Game1.playSound(Unlockable.InteractionSound);

                PrepareExit();
                return false;
            }

            Inventory.removeItemsOfRequirement(who, NextRequirement);
            Unlockable.ProcessContribution(NextRequirement);
            var displayName = NextId == "money" ? NextRequirement.Value.ToString("# ### ##0").TrimStart() + "g" : NextItem.DisplayName;
            Helper.Reflection.GetField<StardewValley.Multiplayer>(typeof(Game1), "multiplayer").GetValue().globalChatInfoMessage("BundleDonate", Game1.player.displayName, displayName);
            AnimationEvent.Fire();
            return true;
        }

        public void ShowYesNo(Farmer who)
        {
            var question = Unlockable.GetTranslatedShopDescription();
            var moneyString = Unlockable.ShopType == ShopType.ParrotPerch ? Helper.Translation.Get("ub_parrot_money") : Helper.Translation.Get("ub_speech_money");

            question = question.Replace("{{item}}", NextId == "money" ? moneyString : NextItem.DisplayName);

            Game1.currentLocation.afterQuestion = ExitYesNo;
            var yesNo = Game1.currentLocation.createYesNoResponses();
            Game1.activeClickableMenu = new DialogueBox(question, yesNo);
        }

        public void ExitYesNo(Farmer who, string whichAnswer)
        {
            if (whichAnswer == "No") {
                PrepareExit();
                return;
            }

            TryDonate(who);
        }

        public virtual void PerformAnimation()
        {
            StateTimer = 3f;

            PerformAnimationLocal();
            AssignNextItem();
        }
        private KeyValuePair<string, Rectangle> GetAnimationTexture(string id, Item item)
        {
            if (id == "money")
                return new("LooseSprites\\Cursors", new Rectangle(280, 412, 15, 14));

            if (item is AdvancedPricingItem apItem)
                return apItem.GetAnimationTexture();

            var itemData = ItemRegistry.GetDataOrErrorItem(id);
            return new(itemData.GetTextureName(), itemData.GetSourceRect());
        }

        private void PerformAnimationLocal()
        {
            if (Game1.currentLocation.NameOrUniqueName != Unlockable.LocationUnique)
                return;

            if (Unlockable.InteractionSound != "")
                Game1.playSound(Unlockable.InteractionSound);

            Parrots.Clear();
            ParrotPresent = true;

            var texture = GetAnimationTexture(NextId, NextItem);

            Game1.currentLocation.temporarySprites.Add(new TemporaryAnimatedSprite(texture.Key, texture.Value, 2000f, 1, 0, (Shop.TileLocation + new Vector2(0.25f, -2.5f)) * 64f, flicker: false, flipped: false, (float)(Shop.TileLocation.Y * 64 + 1) / 10000f, 0f, Color.White, 64f / texture.Value.Width, -0.015f, 0f, 0f) {
                motion = new Vector2(-0.1f, -7f),
                acceleration = new Vector2(0f, 0.25f),
                id = 98765,
                drawAboveAlwaysFront = true
            });
            Game1.playSound("dwop");
            if (Shop.Mutex.IsLockHeld())
                Game1.player.freezePause = 3000;

            TimeUntilChomp = Unlockable.TimeUntilChomp;
            SquawkTime = Unlockable.TimeUntilChomp;
        }

        public virtual void PerformCompleteAnimation()
        {
            //No flash, because, honestly, it is just annoying
            if (Unlockable.LocationUnique == Game1.currentLocation.NameOrUniqueName)
                DelayedAction.playSoundAfterDelay("secret1", 800);
        }

        public void draw(SpriteBatch b)
        {
            DrawSpeechBubble(b);

            DrawPerchParrot(b);

            foreach (var parrot in Parrots)
                parrot.Draw(b);
        }

        public void DrawSpeechBubble(SpriteBatch b)
        {
            if (!IsPlayerNearby || CurrentState.Value > UpgradeState.Idle)
                return;

            Vector2 offset = Unlockable.SpeechBubbleOffset;
            if (CostShakeTime > 0f)
                offset += new Vector2(Utility.RandomFloat(-0.5f, 0.5f) * 4f);

            float yOffset = 4f * (float)Math.Round(Math.Sin(Game1.currentGameTime.TotalGameTime.TotalMilliseconds / 250.0), 2) - 72f;
            Vector2 draw_position = Shop.TileLocation;
            float draw_layer = draw_position.Y * 64f / 10000f;

            //Speechbubble
            b.Draw(Game1.mouseCursors, Game1.GlobalToLocal(Game1.viewport, new Vector2(draw_position.X * 64f, draw_position.Y * 64f - 96f - 48f + yOffset)) + offset, new Rectangle(141, 465, 20, 24), Color.White * 0.75f, 0f, Vector2.Zero, 4f, SpriteEffects.None, draw_layer + 1E-06f);

            Vector2 item_draw_position = Game1.GlobalToLocal(Game1.viewport, new Vector2(draw_position.X * 64f + 8f, draw_position.Y * 64f - 64f - 62f - 8f + yOffset)) + offset;
            if (NextId == "money")
                UtilityMisc.DrawMoneyKiloFormat(b, NextRequirement.Value, (int)item_draw_position.X, (int)item_draw_position.Y, Color.White);
            else {
                NextItem.drawInMenu(b, item_draw_position, 1f);
                //Vanilla doesn't increment the layerDepth, which causes wackiness
                NextItem.DrawMenuIcons(b, item_draw_position, 1f, 1f, 0.90001f, StackDrawType.Draw, Color.White);
            }

        }

        public void DrawPerchParrot(SpriteBatch b)
        {
            if (ParrotPerch == null || !ParrotPresent || CurrentState.Value == UpgradeState.Complete)
                return;

            int num = 0;
            Vector2 zero = Vector2.Zero;
            if (SquawkTime > 0f)
                num = 1;

            if (ShakeTime > 0f)
                zero = new Vector2(Utility.RandomFloat(-0.5f, 0.5f) * 4f);

            b.Draw(ParrotPerch.texture, Game1.GlobalToLocal(Game1.viewport, (Shop.TileLocation + new Vector2(0.5f, -1f)) * 64f) + zero, new Rectangle(num * 24, 24 * Unlockable.ParrotIndex, 24, 24), Color.White, 0f, new Vector2(12f, 16f), 4f, SpriteEffects.None, ((Shop.TileLocation.Y + 1f) * 64f - 1f) / 10000f);
        }
    }

}

