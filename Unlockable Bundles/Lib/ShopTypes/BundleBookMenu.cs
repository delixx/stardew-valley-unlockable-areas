﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using StardewValley;
using StardewValley.BellsAndWhistles;
using StardewValley.Menus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Unlockable_Bundles.ModEntry;

namespace Unlockable_Bundles.Lib.ShopTypes
{
    internal class BundleBookMenu : IClickableMenu
    {
        public Farmer Who;
        public Unlockable Unlockable;
        public List<BundlePage> Pages = new();

        public Texture2D BackgroundTexture;
        public Texture2D JunimoTexture;

        private bool CanClick = true;
        private int Cursor;

        private string HoverText = null;

        public TemporaryAnimatedSpriteList TempSprites = new TemporaryAnimatedSpriteList();

        private const float TargetCompletionAlpha = 0.7f;

        private bool WaitingForShopEvent = false;

        private ItemGrabMenu OutstandingRewards = null;

        private ClickableAnimatedComponent PresentButton;
        private bool ClickableComponentsDirty = false;

        private bool Debug_EnableReopeningCompletedPages = false; //Should be false outside of debugging

        public class BundlePage : ClickableComponent
        {
            public Unlockable Unlockable;
            public AnimatedTexture AnimatedTexture;
            public float Scale = 1f;
            public float MaxShake = 0f;
            public bool ShakeLeft;
            public float Rotation = 0f;
            public float Alpha = 1f;
            public TemporaryAnimatedSprite PackageSprite;
            public bool Completed { get => Unlockable._completed.Value; set => Unlockable._completed.Value = value; }
            public bool DoneWithCompletionAnimation;

            public BundlePage(Unlockable unlockable, BundleBookMenu parentMenu) : base(new Rectangle(), "")
            {
                var texture = Helper.GameContent.Load<Texture2D>(unlockable.ShopTexture);
                Unlockable = unlockable;
                AnimatedTexture = new AnimatedTexture(texture, Unlockable.ShopAnimation, Unlockable.ShopTextureSource);

                if (unlockable.PagePackageIndex >= 0)
                    PackageSprite = new() {
                        texture = parentMenu.BackgroundTexture,
                        sourceRect = new Rectangle(unlockable.PagePackageIndex * 256 % 512, 244 + unlockable.PagePackageIndex * 256 / 512 * 16, 16, 16),
                        sourceRectStartingPos = new(unlockable.PagePackageIndex * 256 % 512, 244 + unlockable.PagePackageIndex * 256 / 512 * 16),
                        interval = 70f,
                        animationLength = 3,
                        totalNumberOfLoops = 99999,
                        position = getBasePosition(parentMenu),
                        layerDepth = 0.8f,
                        color = Color.White,
                        scale = 4f
                    };
            }

            public Vector2 getBasePosition(BundleBookMenu menu)
                => new(menu.xPositionOnScreen + Unlockable.ShopPosition.X * 4f, menu.yPositionOnScreen + Unlockable.ShopPosition.Y * 4f);
        }

        public BundleBookMenu(Farmer who, Unlockable unlockable, IList<Unlockable> pages)
            : base(Game1.uiViewport.Width / 2 - 640, Game1.uiViewport.Height / 2 - 360, 1280, 720, showUpperRightCloseButton: true)
        {
            Game1.playSound("bigSelect");
            Game1.player.CanMove = false;

            JunimoTexture = Game1.temporaryContent.Load<Texture2D>("LooseSprites\\JunimoNote");
            Who = who;
            Unlockable = unlockable;

            if (Unlockable.JunimoNoteTexture == "")
                BackgroundTexture = JunimoTexture;
            else
                BackgroundTexture = Helper.GameContent.Load<Texture2D>(Unlockable.JunimoNoteTexture);

            for (int i = 0; i < pages.Count; i++) {
                BundlePage page = new(pages[i], this) {
                    myID = i + 200,
                    upNeighborID = -99998,
                    rightNeighborID = -99998,
                    leftNeighborID = -99998,
                    downNeighborID = -99998
                };
                checkPagePurchased(page, true);

                Pages.Add(page);
            }

            assignEmptyPagePositions();
            resetUI();

            if (Game1.options.SnappyMenus && Pages.Any()) {
                currentlySnappedComponent = Pages.First();
                snapCursorToCurrentSnappedComponent();
            }
        }

        private void assignEmptyPagePositions()
        {
            int i = 0;
            foreach(var page in Pages.Where(e => e.Unlockable.ShopPosition == Vector2.Zero))
                page.Unlockable.ShopPosition = getBundlePositionFromNumber(i++);
        }

        private static Vector2 getBundlePositionFromNumber(int whichBundle)
        {
            return whichBundle switch {
                0 => new(592 / 4, 136 / 4),
                1 => new(392 / 4, 384 / 4),
                2 => new(784 / 4, 388 / 4),
                3 => new(304 / 4, 252 / 4),
                4 => new(892 / 4, 252 / 4),
                5 => new(588 / 4, 276 / 4),
                6 => new(588 / 4, 380 / 4),
                7 => new(440 / 4, 164 / 4),
                8 => new(776 / 4, 164 / 4),
                _ => new()
            };
        }

        public void resetUI()
        {
            xPositionOnScreen = Game1.uiViewport.Width / 2 - 640;
            yPositionOnScreen = Game1.uiViewport.Height / 2 - 360;

            upperRightCloseButton = new ClickableTextureComponent(new Rectangle(xPositionOnScreen + width - 36, yPositionOnScreen - 8, 48, 48), Game1.mouseCursors, new Rectangle(337, 494, 12, 12), 4f);

            var presentSprite = new TemporaryAnimatedSprite(null, new Rectangle(548, 262, 18, 20), 70f, 4, 99999, new Vector2(-64f, -64f), flicker: false, flipped: false, 0.5f, 0f, Color.White, 4f, 0f, 0f, 0f, local: true) {
                texture = BackgroundTexture
            };
            presentSprite.texture = BackgroundTexture;
            PresentButton = new ClickableAnimatedComponent(new Rectangle(xPositionOnScreen + 592, yPositionOnScreen + 512, 72, 72), "", Game1.content.LoadString("Strings\\StringsFromCSFiles:JunimoNoteMenu.cs.10783"), presentSprite) {
                myID = 100,
                upNeighborID = -99998,
                rightNeighborID = -99998,
                leftNeighborID = -99998,
                downNeighborID = -99998
            };

            foreach (var page in Pages) {
                var basePosition = page.getBasePosition(this);
                if (page.PackageSprite is not null)
                    page.PackageSprite.position = basePosition;

                page.bounds = page.Unlockable.PageIconBounds;
                page.bounds.X += (int)basePosition.X;
                page.bounds.Y += (int)basePosition.Y;
            }

            customPopulateClickableComponentList();
        }

        public void customPopulateClickableComponentList()
        {
            allClickableComponents = new List<ClickableComponent>();
            allClickableComponents.AddRange(Pages);
            allClickableComponents.Add(PresentButton);

            if (Game1.options.SnappyMenus)
                snapToDefaultClickableComponent();
        }

        public override void automaticSnapBehavior(int direction, int oldRegion, int oldID)
        {
            if (ClickableComponentsDirty) {
                ClickableComponentsDirty = false;
                customPopulateClickableComponentList();
            }
            base.automaticSnapBehavior(direction, oldRegion, oldID);
        }

        public override void setUpForGamePadMode() => ClickableComponentsDirty = true;

        public override void gameWindowSizeChanged(Rectangle oldBounds, Rectangle newBounds) => resetUI();

        private bool isReadyToClose() => !WaitingForShopEvent;

        private bool isPresentButtonVisible() => OutstandingRewards is not null;

        public override bool readyToClose()
        {
            if (!isReadyToClose())
                return false;

            GamePadState currentPadState = Game1.input.GetGamePadState();
            KeyboardState keyState = Game1.GetKeyboardState();

            if (((currentPadState.IsButtonDown(Buttons.Start) && !Game1.oldPadState.IsButtonDown(Buttons.Start)) || (currentPadState.IsButtonDown(Buttons.B) && !Game1.oldPadState.IsButtonDown(Buttons.B))))
                exitThisMenu();

            if (keyState.IsKeyDown(Keys.Escape))
                exitThisMenu();

            if (Game1.options.menuButton.Any(e => keyState.IsKeyDown(e.key)))
                exitThisMenu();

            return false;
        }

        public override void receiveLeftClick(int x, int y, bool playSound = true)
        {
            if (!CanClick)
                return;

            if (isPresentButtonVisible() && PresentButton.containsPoint(x, y)) {
                Game1.activeClickableMenu = OutstandingRewards;
                OutstandingRewards = null;
                return;
            }

            if (upperRightCloseButton.containsPoint(x, y) && isReadyToClose())
                exitThisMenu();

            foreach (var page in Pages) {
                if ((!page.Completed || Debug_EnableReopeningCompletedPages) && isHoveringPage(page, x, y)) {
                    HoverText = null;
                    Game1.activeClickableMenu = new BundlePageMenu(Who, page.Unlockable, this) {
                        exitFunction = () => {
                            checkPagePurchased(page, false);
                            if (Game1.options.SnappyMenus) {
                                currentlySnappedComponent = isPresentButtonVisible() ? PresentButton : page;
                                snapCursorToCurrentSnappedComponent();
                            }
                        }
                    };
                    return;
                }
            }
        }


        //We call processShopEvent with a delay if the bundle has a ShopEvent
        //This is so the completed animation is still shown
        public void checkBookCompleted(BundlePage page)
        {
            page.DoneWithCompletionAnimation = true;

            if (WaitingForShopEvent)
                if (Pages.All(e => e.DoneWithCompletionAnimation) && page.Unlockable.HasShopEvent())
                    doSensibleShopEvent(page.Unlockable, true);

                else if (page.Unlockable.HasShopEvent())
                    doSensibleShopEvent(page.Unlockable);

                else {
                    doSensibleShopEvent(page.Unlockable);
                    doSensibleShopEvent(Unlockable);
                }

            CanClick = true;
            WaitingForShopEvent = false;
        }

        public void checkPagePurchased(BundlePage page, bool duringBookMenuOpen)
        {
            Game1.activeClickableMenu = this;

            if (!duringBookMenuOpen && page.Completed) {
                var allCompleted = Pages.All(e => e.Completed);
                var waitForAnimationToEnd = page.Unlockable.HasShopEvent() || (allCompleted && Unlockable.HasShopEvent());

                if (waitForAnimationToEnd) {
                    CanClick = false;
                    WaitingForShopEvent = true;

                } else
                    doSensibleShopEvent(page.Unlockable);

                if (allCompleted) {
                    Unlockable.ProcessPurchase();

                    if (!waitForAnimationToEnd)
                        doSensibleShopEvent(Unlockable);
                }
            }

            if (page.PackageSprite is not null)
                checkPagePurchasedForPackage(page, duringBookMenuOpen);

            else
                checkPagePurchasedForShopTexture(page, duringBookMenuOpen);
        }

        private bool doSensibleShopEvent(Unlockable unlockable, bool appendBookEvent = false)
        {
            unlockable.ProcessShopEvent(postEventHandle);

            void postEventHandle()
            {
                if (Game1.activeClickableMenu is ItemGrabMenu rewards && OutstandingRewards is not null)
                    foreach (var item in OutstandingRewards.ItemsToGrabMenu.actualInventory)
                        rewards.ItemsToGrabMenu.actualInventory.Add(item);

                if (appendBookEvent) {
                    if (Game1.activeClickableMenu is ItemGrabMenu rewards2) {
                        OutstandingRewards = rewards2;
                        Game1.activeClickableMenu = null;
                    }

                    doSensibleShopEvent(Unlockable);
                }
            }

            if (!appendBookEvent && !unlockable.HasShopEvent() && Game1.activeClickableMenu is ItemGrabMenu rewards) {
                if (OutstandingRewards is not null)
                    foreach (var item in rewards.ItemsToGrabMenu.actualInventory)
                        OutstandingRewards.ItemsToGrabMenu.actualInventory.Add(item);
                else
                    OutstandingRewards = rewards;

                Game1.activeClickableMenu = this;
            }

            return Game1.activeClickableMenu == this;
        }

        private void checkPagePurchasedForShopTexture(BundlePage page, bool duringBookMenuOpen)
        {
            if (!page.Completed)
                return;

            if (!string.IsNullOrWhiteSpace(page.Unlockable.ShopCompletedAnimation))
                page.AnimatedTexture = new AnimatedTexture(page.AnimatedTexture.Texture, page.Unlockable.ShopCompletedAnimation, page.Unlockable.ShopTextureSource);

            if (duringBookMenuOpen) {
                page.Alpha = TargetCompletionAlpha;
                page.MaxShake = (float)Math.PI * 3f / 128f;
                page.DoneWithCompletionAnimation = true;

            } else {
                Game1.playSound("dwop");
                Task.Delay(800).ContinueWith(t => playLeafRustle(page));
                var bounds = new Rectangle((int)page.getBasePosition(this).X + page.Unlockable.PageIconBounds.X - 64, (int)page.getBasePosition(this).Y + page.Unlockable.PageIconBounds.Y - 64, page.Unlockable.PageIconBounds.Width + 64, page.Unlockable.PageIconBounds.Height + 64);
                TempSprites.AddRange(Utility.sparkleWithinArea(bounds, 8, page.Unlockable.PageLeafRustleColor * 0.5f));

            }
        }

        private void checkPagePurchasedForPackage(BundlePage page, bool duringBookMenuOpen)
        {
            page.PackageSprite.reset();
            page.PackageSprite.sourceRect.X += page.PackageSprite.sourceRect.Width;
            page.PackageSprite.paused = true;

            if (!page.Completed)
                return;

            completionAnimationForPackage(page, duringBookMenuOpen);
        }

        public override void performHoverAction(int x, int y)
        {
            base.performHoverAction(x, y);

            HoverText = null;
            if (isPresentButtonVisible())
                HoverText = PresentButton.tryHover(x, y);

            foreach (var page in Pages)
                tryHoverPage(x, y, page);
        }

        private void tryHoverPage(int x, int y, BundlePage page)
        {
            if (page.Completed)
                return;

            var maxScaleIncrease = 0.1f;

            if (isHoveringPage(page, x, y)) {
                HoverText = page.Unlockable.BundleName == "" ? "" : page.Unlockable.GetDisplayName();

                if (page.PackageSprite is null)
                    page.Scale = Math.Min(page.Scale + 0.04f, 1f + maxScaleIncrease);

                else if (!page.Completed)
                    page.PackageSprite.paused = false;

            } else {
                if (page.PackageSprite is null)
                    page.Scale = Math.Max(page.Scale - 0.04f, 1f);

                else if (!page.Completed) {
                    page.PackageSprite.reset();
                    page.PackageSprite.sourceRect.X += page.PackageSprite.sourceRect.Width;
                    page.PackageSprite.paused = true;

                }
            }
        }

        private bool isHoveringPage(BundlePage page, int x, int y)
        {
            //var position = page.getBasePosition(this);
            //return page.Unlockable.PageIconBounds.Contains(x - position.X, y - position.Y);
            return page.bounds.Contains(x, y);
        }

        public new void exitThisMenu(bool playSound = true)
        {
            Game1.dialogueUp = false;
            Game1.player.CanMove = true;
            base.exitThisMenu(playSound);

            if (OutstandingRewards is not null)
                Game1.activeClickableMenu = OutstandingRewards;
        }

        public override void update(GameTime time)
        {
            foreach (var page in Pages)
                updatePage(time, page);

            for (int i = TempSprites.Count - 1; i >= 0; i--)
                if (TempSprites[i].update(time))
                    TempSprites.RemoveAt(i);

            PresentButton.update(time);
        }

        private void updatePage(GameTime time, BundlePage page)
        {
            page.PackageSprite?.update(time);

            if (page.MaxShake > 0f) {
                if (page.ShakeLeft) {
                    page.Rotation -= (float)Math.PI / 200f;
                    if (page.Rotation <= 0f - page.MaxShake)
                        page.ShakeLeft = false;

                } else {
                    page.Rotation += (float)Math.PI / 200f;
                    if (page.Rotation >= page.MaxShake)
                        page.ShakeLeft = true;
                }
            } else
                page.Rotation = 0f;

            if (page.PackageSprite is not null)
                page.PackageSprite.rotation = page.Rotation;

            if (page.MaxShake > 0f)
                page.MaxShake = Math.Max(0f, page.MaxShake - (page.PackageSprite is null ? 0.001f : 0.0007669904f));

            if (page.Completed && page.Alpha > TargetCompletionAlpha)
                page.Alpha = Math.Max(TargetCompletionAlpha, page.Alpha - 0.01f);
        }

        private void playLeafRustle(BundlePage page)
        {
            page.MaxShake = (float)Math.PI * 3f / 128f;
            Game1.playSound("leafrustle");
            var position = page.getBasePosition(this);

            if (page.PackageSprite is null) {
                position.X += page.Unlockable.PageIconBounds.X;
                position.Y += page.Unlockable.PageIconBounds.Y;
            }

            TemporaryAnimatedSprite tempSprite = new TemporaryAnimatedSprite(50, position, page.Unlockable.PageLeafRustleColor) {
                motion = new Vector2(-1f, 0.5f),
                acceleration = new Vector2(0f, 0.02f)
            };
            tempSprite.sourceRect.Y++;
            tempSprite.sourceRect.Height--;
            TempSprites.Add(tempSprite);

            tempSprite = new TemporaryAnimatedSprite(50, position, page.Unlockable.PageLeafRustleColor) {
                motion = new Vector2(1f, 0.5f),
                acceleration = new Vector2(0f, 0.02f),
                flipped = true,
                delayBeforeAnimationStart = 50
            };
            tempSprite.sourceRect.Y++;
            tempSprite.sourceRect.Height--;
            TempSprites.Add(tempSprite);

            Task.Delay(1500).ContinueWith(e => checkBookCompleted(page));
        }

        public override void draw(SpriteBatch b)
        {
            Cursor = -1;
            b.Draw(Game1.fadeToBlackRect, new Rectangle(0, 0, Game1.uiViewport.Width, Game1.uiViewport.Height), Color.Black * 0.5f);

            drawMenu(b);
            drawDescription(b);
            drawPageIcons(b);

            if (isPresentButtonVisible())
                PresentButton.draw(b);

            foreach (var tempSprite in TempSprites)
                tempSprite.draw(b, localPosition: true);

            drawHoverPage(b);

            upperRightCloseButton.draw(b);

            if (CanClick)
                drawMouse(b, cursor: Cursor);
        }

        private void drawMenu(SpriteBatch b)
        {
            b.Draw(BackgroundTexture, new Vector2(xPositionOnScreen, yPositionOnScreen), new Rectangle(0, 0, 320, 180), Color.White, 0f, Vector2.Zero, 4f, SpriteEffects.None, 0.1f);
        }
        public void drawDescription(SpriteBatch b)
        {
            if (!string.IsNullOrEmpty(Unlockable.BundleName))
                SpriteText.drawStringHorizontallyCenteredAt(b, Unlockable.GetDisplayName(), xPositionOnScreen + width / 2 + 16, yPositionOnScreen + 12, 999999, -1, 99999, 0.88f, 0.88f);

            if (Unlockable.BundleDescription != "")
                SpriteText.drawStringWithScrollCenteredAt(b, Unlockable.BundleDescription, xPositionOnScreen + width / 2, Math.Min(yPositionOnScreen + height + 20, Game1.uiViewport.Height - 64 - 8));
        }

        private void drawHoverPage(SpriteBatch b)
        {
            if (HoverText is null)
                return;

            Cursor = 44;
            if (HoverText != "")
                drawHoverText(b, HoverText, Game1.dialogueFont);
        }
        private void drawPageIcons(SpriteBatch b)
        {
            foreach (var page in Pages) {
                var position = page.getBasePosition(this);

                if (page.PackageSprite is null) {
                    var hoverOffset = ((Unlockable.ShopDrawDimensions * page.Scale) - Unlockable.ShopDrawDimensions) / 2;
                    b.Draw(
                        texture: page.AnimatedTexture.Texture,
                        destinationRectangle: new Rectangle((position + page.Unlockable.ShopDrawOffset - hoverOffset).ToPoint(), (page.Unlockable.ShopDrawDimensions * page.Scale).ToPoint()),
                        sourceRectangle: page.AnimatedTexture.GetOffsetRectangle(),
                        color: Color.White * page.Alpha,
                        rotation: page.Rotation,
                        origin: new Vector2(),
                        effects: SpriteEffects.None,
                        1f);

                    page.AnimatedTexture.Update(Game1.currentGameTime);

                } else
                    page.PackageSprite.draw(b, true, yOffset: 64);


            }
        }

        private void completionAnimationForPackage(BundlePage page, bool duringBookMenuOpen)
        {
            page.PackageSprite.pingPong = false;
            page.PackageSprite.paused = false;
            page.PackageSprite.sourceRect.X = (int)page.PackageSprite.sourceRectStartingPos.X;
            page.PackageSprite.sourceRect.X += page.PackageSprite.sourceRect.Width;
            page.PackageSprite.animationLength = 15;
            page.PackageSprite.interval = 50f;
            page.PackageSprite.totalNumberOfLoops = 0;
            page.PackageSprite.holdLastFrame = true;
            page.PackageSprite.extraInfoForEndBehavior = 1;

            if (!duringBookMenuOpen) {
                Game1.playSound("dwop");
                page.PackageSprite.endFunction = e => playLeafRustle(page);
                TempSprites.AddRange(Utility.sparkleWithinArea(new((int)page.PackageSprite.Position.X - 64, (int)page.PackageSprite.Position.Y - 64, 128, 128), 8, page.Unlockable.PageLeafRustleColor * 0.5f));

            } else {
                page.DoneWithCompletionAnimation = true;

                page.MaxShake = (float)Math.PI * 3f / 128f;
                page.PackageSprite.sourceRect.X += page.PackageSprite.sourceRect.Width * 14;
                page.PackageSprite.sourceRectStartingPos = new Vector2(page.PackageSprite.sourceRect.X, page.PackageSprite.sourceRect.Y);
                page.PackageSprite.currentParentTileIndex = 14;
                page.PackageSprite.interval = 0f;
                page.PackageSprite.animationLength = 1;
                page.PackageSprite.extraInfoForEndBehavior = 0;

            }
        }
    }
}
