﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewValley;
using StardewModdingAPI;

namespace Unlockable_Bundles.Lib.ShopTypes
{
    public class Main
    {
        public static void Initialize()
        {
            Inventory.Initialize();
            BundlePageMenu.Initialize();
            DialogueShopMenu.Initialize();
            SpeechBubble.Initialize();
        }
    }
}
