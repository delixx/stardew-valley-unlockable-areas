﻿using HarmonyLib;
using StardewModdingAPI;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Unlockable_Bundles.ModEntry;

namespace Unlockable_Bundles.Lib
{
    internal class _GameLocation
    {
        public static void Initialize()
        {
            var harmony = new Harmony(ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(GameLocation), nameof(GameLocation.reloadMap)),
                postfix: new HarmonyMethod(typeof(_GameLocation), nameof(_GameLocation.ReloadMap_Postfix))
            );

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(GameLocation), nameof(GameLocation.MakeMapModifications), new[] { typeof(bool) }),
                postfix: new HarmonyMethod(typeof(_GameLocation).GetMethod(nameof(_GameLocation.MakeMapModifications_Postfix)), after: new[] { "aedenthorn.MapEdit" })
            );
        }

        public static void ReloadMap_Postfix(GameLocation __instance)
        {
            MapPatches.CheckIfMapPatchesNeedToBeReapplied(__instance, "reloadMap");
        }

        public static void MakeMapModifications_Postfix(GameLocation __instance, bool force)
        {
            if (!Helper.ModRegistry.IsLoaded("aedenthorn.MapEdit"))
                return;

            MapPatches.CheckIfMapPatchesNeedToBeReapplied(__instance, "MakeMapModifications");
        }
    }
}
