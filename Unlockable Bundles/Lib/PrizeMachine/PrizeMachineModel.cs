﻿using StardewValley.GameData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib.PrizeMachine
{
    internal class PrizeMachineModel
    {
        public string Id;
        public Dictionary<int, PrizeMachineItem> GuarenteedRewards = new();
        public string MenuTexture = "LooseSprites\\PrizeTicketMenu";
        public PrizeMachineItem Cost;

        public Dictionary<string, int> RandomRewards = new();
        public List<GenericSpawnItemDataWithCondition> RandomRewardSpawnFields = new();
    }

    internal class PrizeMachineItem
    {
        public string ItemId { get; set; }
        public int Amount { get; set; }
    }
}
