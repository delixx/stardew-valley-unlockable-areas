﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib.PrizeMachine
{
    public class PrizeMachineSaveData
    {
        public Dictionary<int, string> ItemTrack = new();
        public int PrizeLevel;
    }
}
