﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using StardewModdingAPI;
using StardewValley;
using StardewValley.BellsAndWhistles;
using StardewValley.GameData;
using StardewValley.Internal;
using StardewValley.Menus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.Lib.AdvancedPricing;
using Unlockable_Bundles.Lib.ShopTypes;
using static Unlockable_Bundles.ModEntry;

namespace Unlockable_Bundles.Lib.PrizeMachine
{
    internal class PrizeMachineMenu : IClickableMenu
    {
        private PrizeMachineSaveData SaveData;
        private Texture2D Texture;
        private PrizeMachineModel Machine;

        public const int Width = 116;

        public const int Height = 94;

        private ClickableTextureComponent MainButton;

        private float PressedButtonTimer;

        private List<Item> CurrentPrizeTrack = new List<Item>();

        private float GetRewardTimer;

        private float MoveRewardTrackTimer;

        private float MoveRewardTrackPreTimer;

        private bool GettingReward;

        private bool MovingRewardTrack;

        public PrizeMachineMenu(string prizeMachineId, PrizeMachineModel machine)
            : base((int)Utility.getTopLeftPositionForCenteringOnScreen(464, 376).X, (int)Utility.getTopLeftPositionForCenteringOnScreen(464, 376).Y, 464, 376, showUpperRightCloseButton: true)
        {
            SaveData = ModData.GetPrizeMachineSaveData(prizeMachineId, Game1.player.UniqueMultiplayerID);
            Machine = machine;

            Texture = Game1.content.Load<Texture2D>(machine.MenuTexture);
            MainButton = new ClickableTextureComponent(new Rectangle(xPositionOnScreen + 192, yPositionOnScreen + 216, 92, 88), Texture, new Rectangle(150, 29, 23, 22), 4f);
            Game1.playSound("machine_bell");
            addPrizeItem(SaveData.PrizeLevel + 0);
            addPrizeItem(SaveData.PrizeLevel + 1);
            addPrizeItem(SaveData.PrizeLevel + 2);
            addPrizeItem(SaveData.PrizeLevel + 3);

            if (Game1.options.SnappyMenus) {
                currentlySnappedComponent = MainButton;
                snapCursorToCurrentSnappedComponent();
            }
        }

        private void addPrizeItem(int prizeLevel)
        {
            if (Machine.GuarenteedRewards.TryGetValue(prizeLevel, out var machineItem)) {
                var item = parseRewardItem(machineItem.ItemId, machineItem.Amount);
                if (item is not null) {
                    CurrentPrizeTrack.Add(item);
                    return;
                }
            }

            if (SaveData.ItemTrack.TryGetValue(prizeLevel, out var serializedItem)) {
                Item item = serializedItem.DeserializeXML<ParsableItem>();
                if (item is not null) {
                    CurrentPrizeTrack.Add(item);
                    return;
                }
            }

            List<Item> rewards = new List<Item>();

            foreach (var reward in Machine.RandomRewards) {
                var item = parseRewardItem(reward.Key, reward.Value);
                if (item is not null) {
                    rewards.Add(item);
                    continue;
                }
            }

            rewards.AddRange(getRewardSpawnFieldItems());

            if(rewards.Count == 0) {
                Monitor.Log("No Random Prizes. Filling with wood to avoid errors", LogLevel.Error);
                rewards.Add(ItemRegistry.Create("(O)388)"));
            }

            Random r = Utility.CreateRandom(Game1.uniqueIDForThisGame, Game1.player.UniqueMultiplayerID, prizeLevel);
            var randomItem = rewards[r.Next(rewards.Count)];

            CurrentPrizeTrack.Add(randomItem);
            SaveData.ItemTrack[prizeLevel] = ((ParsableItem)randomItem).SerializeXML();
        }

        private static Item parseRewardItem(string itemId, int amount)
        {
            var id = Unlockable.GetIDFromReqSplit(itemId);
            var quality = Unlockable.GetQualityFromReqSplit(itemId);

            if (id.ToLower().Trim() == "money") {
                Monitor.Log($"PrizeMachine rewards does currently not support money. Use (O)GoldCoin instead", LogLevel.Warn);
                return null;
            }

            var item = Unlockable.ParseItem(id, amount, quality: quality);
            if (item is not AdvancedPricingItem apItem)
                return item;

            if (apItem.UsesFlavoredSyntax) {
                apItem.ItemCopy.Quality = quality;
                apItem.ItemCopy.Stack = amount;
                return apItem.ItemCopy;

            } else {
                Monitor.Log($"PrizeMachine rewards does not accept advanced pricing syntax apart from auto generated flavored Items! Please fix the following ItemID: {id}", LogLevel.Error);
                return null;
            }
        }

        public List<Item> getRewardSpawnFieldItems()
        {
            var ret = new List<Item>();
            ItemQueryContext itemQueryContext = new();
            foreach (var entry in Machine.RandomRewardSpawnFields) {
                if (!GameStateQuery.CheckConditions(entry.Condition))
                    continue;

                var item = ItemQueryResolver.TryResolveRandomItem(entry, itemQueryContext, logError: (query, message) => Monitor.Log($"Failed parsing item query for prize machine '{query}': {message}", LogLevel.Warn));
                if (item is not null)
                    ret.Add(item);
            }
            return ret;
        }

        public override void performHoverAction(int x, int y)
        {
            if (MainButton.containsPoint(x, y) && PressedButtonTimer <= 0f && !GettingReward && !MovingRewardTrack) {
                if (MainButton.sourceRect.Y == 29) {
                    Game1.playSound("button_tap");
                }
                MainButton.sourceRect.Y = 51;
            } else {
                MainButton.sourceRect.Y = 29;
            }
            base.performHoverAction(x, y);
        }

        public override bool readyToClose()
        {
            if (!GettingReward) {
                return base.readyToClose();
            }
            return false;
        }

        public override void receiveLeftClick(int x, int y, bool playSound = true)
        {
            if (GettingReward) {
                return;
            }
            if (MainButton.containsPoint(x, y) && PressedButtonTimer <= 0f && !MovingRewardTrack) {
                Game1.playSound("button_press");
                PressedButtonTimer = 200f;

                var count = Inventory.getItemCount(Game1.player, Machine.Cost.ItemId);
                if (count >= Machine.Cost.Amount) {
                    Inventory.removeItemsOfRequirement(Game1.player, new(Machine.Cost.ItemId, Machine.Cost.Amount));
                    GettingReward = true;
                    GetRewardTimer = 0f;
                    DelayedAction.playSoundAfterDelay("discoverMineral", 750);
                }
            }
            base.receiveLeftClick(x, y, playSound);
        }

        public override void update(GameTime time)
        {
            if (PressedButtonTimer > 0f) {
                PressedButtonTimer -= (int)time.ElapsedGameTime.TotalMilliseconds;
                MainButton.sourceRect.Y = 73;
            }
            if (PressedButtonTimer <= 0f && GettingReward) {
                GetRewardTimer += (float)time.ElapsedGameTime.TotalMilliseconds;
                if (GetRewardTimer > 2000f) {
                    GetRewardTimer = 2000f;
                    Game1.playSound("coin");

                    if (CurrentPrizeTrack[0].QualifiedItemId == "(O)GoldCoin") {
                        var coinTotal = CurrentPrizeTrack[0].Stack * 250;
                        Game1.player.Money += coinTotal;
                        Game1.dayTimeMoneyBox.gotGoldCoin(coinTotal);
                    }              
                    else if (!Game1.player.addItemToInventoryBool(CurrentPrizeTrack[0]))
                        Game1.createItemDebris(CurrentPrizeTrack[0], Game1.player.getStandingPosition(), 1, Game1.player.currentLocation);

                    SaveData.ItemTrack.Remove(SaveData.PrizeLevel);
                    SaveData.PrizeLevel += 1;
                    CurrentPrizeTrack.RemoveAt(0);
                    MoveRewardTrackPreTimer = 500f;
                    GettingReward = false;
                    MovingRewardTrack = true;
                    MoveRewardTrackTimer = 0f;
                }
            } else if (MovingRewardTrack) {
                if (MoveRewardTrackPreTimer > 0f) {
                    MoveRewardTrackPreTimer -= (float)time.ElapsedGameTime.TotalMilliseconds;
                    if (MoveRewardTrackPreTimer <= 0f) {
                        Game1.playSound("ticket_machine_whir");
                    }
                } else {
                    MoveRewardTrackTimer += (float)time.ElapsedGameTime.TotalMilliseconds;
                    if (MoveRewardTrackTimer >= 2000f) {
                        MovingRewardTrack = false;
                        addPrizeItem(SaveData.PrizeLevel + 3);
                    }
                }
            }
            base.update(time);
        }

        public override void draw(SpriteBatch b)
        {
            if (!Game1.options.showClearBackgrounds) {
                b.Draw(Game1.fadeToBlackRect, Game1.graphics.GraphicsDevice.Viewport.Bounds, Color.Black * 0.6f);
            }
            b.Draw(Texture, new Vector2(xPositionOnScreen, yPositionOnScreen) + new Vector2(25f, 18f) * 4f, new Rectangle(0, 106, 76, 22), Color.White, 0f, Vector2.Zero, 4f, SpriteEffects.None, 0.6f);
            for (int i = 0; i < CurrentPrizeTrack.Count; i++) {
                Vector2 posOffset = new Vector2(28 + 22 * i, 21f) * 4f;
                if (MovingRewardTrack) {
                    float xOffset = 88f - MoveRewardTrackTimer / 18f;
                    if (xOffset > 0f) {
                        posOffset.X += xOffset;
                        if (MoveRewardTrackPreTimer <= 0f) {
                            posOffset.X += Game1.random.Next(-1, 2);
                            posOffset.Y += Game1.random.Next(-1, 2);
                        }
                    }
                }
                if (i == 0) {
                    b.Draw(Game1.fadeToBlackRect, new Rectangle((int)Position.X + 100, (int)Position.Y + 76, 88, 80), Color.LightYellow * 0.33f);
                }
                if (!GettingReward || i != 0) {
                    CurrentPrizeTrack[i].drawInMenu(b, Position + posOffset, 1f);
                }
            }
            b.Draw(Texture, new Vector2(xPositionOnScreen, yPositionOnScreen), new Rectangle(0, 0, 116, 94), Color.White, 0f, Vector2.Zero, 4f, SpriteEffects.None, 0.87f);
            if (GettingReward) {
                Vector2 posOffset2 = new Vector2(28f, 21f) * 4f;
                posOffset2.Y -= GetRewardTimer / 13f;
                posOffset2.Y = Math.Max(posOffset2.Y, 0f);
                posOffset2.X += GetRewardTimer / 1000f * (float)Game1.random.Next(-1, 2);
                posOffset2.Y += GetRewardTimer / 1000f * (float)Game1.random.Next(-1, 2);
                CurrentPrizeTrack[0].drawInMenu(b, Position + posOffset2, 1f, 1f, 0.9f, StackDrawType.Draw, Color.White, drawShadow: false);
            }
            string ticketCount = Inventory.getItemCount(Game1.player, Machine.Cost.ItemId).ToString() ?? "";
            SpriteText.drawString(b, ticketCount, xPositionOnScreen + 360 - SpriteText.getWidthOfString(ticketCount) / 2, yPositionOnScreen + 276);
            MainButton.draw(b);
            base.draw(b);
            base.drawMouse(b);
        }
    }
}
