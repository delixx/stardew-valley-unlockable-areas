﻿using Microsoft.Xna.Framework;
using StardewModdingAPI;
using StardewValley;
using StardewValley.Delegates;
using StardewValley.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Unlockable_Bundles.ModEntry;

namespace Unlockable_Bundles.Lib.PrizeMachine
{
    internal class PrizeMachineHandler
    {
        public const string Asset = "UnlockableBundles/PrizeTicketMachines";

        public static void Initialize()
        {
            GameLocation.RegisterTileAction("UB_OpenPrizeMachine", OpenPrizeMachine);
        }

        private static bool OpenPrizeMachine(GameLocation location, string[] args, Farmer player, Point tile)
        {
            if (!ArgUtility.TryGet(args, 1, out string prizeMachineId, out var error, allowBlank: false)) {
                Monitor.Log(error, LogLevel.Error);
                return false;
            }

            var machine = getMachineById(prizeMachineId);
            if (machine is null)
                return false;

            Game1.activeClickableMenu = new PrizeMachineMenu(prizeMachineId, machine);
            return true;
        }

        public static PrizeMachineModel getMachineById(string prizeMachineId, bool expectedToExist = true)
        {
            var machines = Helper.GameContent.Load<Dictionary<string, PrizeMachineModel>>(Asset);
            if (!machines.TryGetValue(prizeMachineId, out var machine)) {
                if (expectedToExist)
                    Monitor.Log($"Invalid Prize Machine Id {prizeMachineId}", LogLevel.Error);
                return null;
            }

            machine.Id = prizeMachineId;
            return machine;
        }
    }
}
