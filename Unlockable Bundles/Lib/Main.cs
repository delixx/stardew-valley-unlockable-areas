﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewModdingAPI;
using StardewValley;
using Unlockable_Bundles.Lib.MapFeatures;

namespace Unlockable_Bundles.Lib
{
    public class Main
    {
        public static void Initialize()
        {
            ShopTypes.Main.Initialize();
            AdvancedPricing.Main.Initialize();
            MapFeatures.Main.Initialize();
            WalletCurrency.Main.Initialize();
            Quests.Main.Initialize();
            PrizeMachine.Main.Initialize();

            ConsoleCommands.Initialize();
            AssetRequested.Initialize();
            SaveDataEvents.Initialize();
            ShopObject.Initialize();
            ShopPlacement.Initialize();
            MapPatches.Initialize();
            UtilityMisc.Initialize();
            UnsafeMap.Initialize();
            UBEvent.Initialize();
            _InventoryPage.Initialize();
            _GameLocation.Initialize();
            DonatableItemTooltip.Initialize();
            BundleOverviewMenu.Initialize();
            PlacementRequirement.Initialize();
            Multiplayer.Initialize();
        }
    }
}
