﻿using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewModdingAPI.Utilities;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.Lib.Enums;
using Unlockable_Bundles.Lib.MapFeatures;
using Unlockable_Bundles.Lib.ShopTypes;
using Unlockable_Bundles.Lib.WalletCurrency;
using Unlockable_Bundles.NetLib;
using static Unlockable_Bundles.ModEntry;

namespace Unlockable_Bundles.Lib
{
    internal class Multiplayer
    {
        public static PerScreen<bool> IsScreenReady = new();
        public static void Initialize()
        {
            Helper.Events.Multiplayer.ModMessageReceived += ModMessageReceived;
            Helper.Events.Multiplayer.PeerConnected += PeerConnected;
        }

        public static string GetDebugName() => $"{(Context.IsOnHostComputer ? "P" + Context.ScreenId : "NotOnHostComputer")} {Game1.player.UniqueMultiplayerID}";

        private static void PeerConnected(object sender, PeerConnectedEventArgs e)
        {
            if (!Context.IsMainPlayer)
                return;

            var saveData = ModData.Instance.UnlockableSaveData;
            var unlockables = Helper.GameContent.Load<Dictionary<string, UnlockableModel>>("UnlockableBundles/Bundles");
            List<UnlockableModel> applyList = new();

            foreach (var keyDicPairs in saveData)
                foreach (var locationValue in keyDicPairs.Value)
                    if (unlockables.TryGetValue(keyDicPairs.Key, out UnlockableModel unlockable)) {
                        unlockable.ID = keyDicPairs.Key;
                        unlockable.LocationUnique = locationValue.Key;
                        unlockable.ApplyDefaultValues();

                        if (locationValue.Value.Purchased)
                            applyList.Add((UnlockableModel)new Unlockable(unlockable)); //Cloning
                    }

            Helper.Multiplayer.SendMessage(new KeyValuePair<List<UnlockableModel>, ModData>(applyList, ModData.Instance), "UnlockablesReady", modIDs: new[] { ModManifest.UniqueID }, playerIDs: new[] { e.Peer.PlayerID });
            Helper.Multiplayer.SendMessage(AssetRequested.MailData, "UpdateMailData", modIDs: new[] { ModManifest.UniqueID });
        }

        private static void ModMessageReceived(object sender, ModMessageReceivedEventArgs e)
        {
            if (e.FromModID != ModManifest.UniqueID)
                return;
            switch (e.Type) {
                case "UnlockablesReady": //Sent to clients to inform that the host is done setting up
                    Received_UnlockablesReady(e); break;

                case "BundlePurchased": //Broadcast after bundle purchase
                    Received_BundlePurchased(e); break;

                case "BundleContributed": //Broadcast after bundle contribution
                    Received_BundleContributed(e); break;

                case "BundleDiscovered": //Broadcast after bundle discovery
                    Received_BundleDiscovered(e); break;

                case "RequestBundleRefresh": //Host receiving request for the bundle data they don't have access to directly. This only returns discovered bundles
                    Received_RequestBundleRefresh(e); break;

                case "SendBundleRefresh": //Client receiving the bundle data they don't have access to directly to refresh
                    Received_SendBundleRefresh(e); break;

                case "UpdateMailData": //Host sending relevant mail data to clients. Usually alongside UnlockablesReady
                    Received_UpdateMailData(e); break;

                case "ReapplyLocation": //Splitscreen client urging host to reapply all map patches for a location. Circumvents odd, inexplicable splitscreen behavior
                    Received_ReapplyLocation(e); break;

                case "DebugWarpToHost": //Warps the user to the hosts location. Undocumented and strictly used to make debugging easier
                    Received_DebugWarpToHost(e); break;

                case "SPRUpdated": //Client informs host that a SPR condition has changed (eg. a TriggerAction condition was met by client)
                    Received_SPRUpdated(e); break;

                case "ResetDigSpot": //Broadcast to inform that a dig spot should be reset, eg. after a UB_ResetDigSpot TriggerAction
                    Received_ResetDigSpot(e); break;

                case "WalletCurrencyChanged": //Broadcast to inform about any wallet currency changes
                    Received_WalletCurrencyChanged(e); break;

                case "BundleFavoriteUpdated": //Client changed bundle favorite in overview menu
                    Received_BundleFavoriteUpdated(e); break;
            }
        }

        private static void Received_UnlockablesReady(ModMessageReceivedEventArgs e)
        {
            if (!Context.IsOnHostComputer)
                ShopPlacement.ResetDay();

            //I have 0 control over when this is run and whether my bundles asset is loaded at this point or not
            //So instead of reading the bundles asset and using ModData for Unlockable data, I transfer all unlockables that need to be applied
            //While mighty unfortunate, I'd rather have lots of redundancy than unreliable and finicky architecture
            //Eventually if this framework becomes popular enough for this to be a performance concern,
            //I could create a new transfer model with all of the fields applyUnlockable needs
            var transferData = e.ReadAs<KeyValuePair<List<UnlockableModel>, ModData>>();

            if (!Context.IsOnHostComputer)
                ModData.Instance = transferData.Value;
            var applyList = transferData.Key;

            //If the world isn't ready at this point (which it might be) the map patches aren't being applied
            //In that case we apply them in ShopPlacement.DayStarted
            Monitor.Log($"{GetDebugName()} IsWorldReady: {Context.IsWorldReady}", DebugLogLevel);

            if (Context.IsWorldReady)
                foreach (var unlockable in applyList)
                    MapPatches.ApplyUnlockable(new Unlockable(unlockable), !Context.IsOnHostComputer);
            else
                ShopPlacement.UnappliedMapPatches.Value = applyList;

            ModAPI.raiseIsReady(new API.IsReadyEventArgs(Game1.player));
        }

        private static void Received_BundlePurchased(ModMessageReceivedEventArgs e)
        {
            var unlockable = new Unlockable(e.ReadAs<UnlockableModel>());

            ModData.SetPurchased(unlockable.ID, unlockable.LocationUnique);
            ModAPI.RaiseShopPurchased(new API.BundlePurchasedEventArgs(Game1.player, unlockable.Location, unlockable.LocationUnique, unlockable.ID, false));

            MapPatches.ApplyUnlockable(unlockable, !MapPatches.AppliedUnlockables.Any(el => el.ID == unlockable.ID && el.LocationUnique == unlockable.LocationUnique));

            if (Game1.activeClickableMenu != null
                && Game1.activeClickableMenu.GetType() == typeof(DialogueShopMenu)
                && (Game1.activeClickableMenu as DialogueShopMenu).Unlockable.ID == unlockable.ID)
                Game1.activeClickableMenu.exitThisMenu();
        }

        private static void Received_BundleContributed(ModMessageReceivedEventArgs e)
        {
            var unlockable = new Unlockable(e.ReadAs<UnlockableModel>());

            var last = unlockable._alreadyPaid.Pairs.Last();
            var index = unlockable._alreadyPaidIndex.ContainsKey(last.Key) ? unlockable._alreadyPaidIndex[last.Key] : -1;
            ModData.SetPartiallyPurchased(unlockable.ID, unlockable.LocationUnique, last.Key, last.Value, index);
            ModAPI.raiseShopContributed(new API.BundleContributedEventArgs(Game1.player, new KeyValuePair<string, int>(last.Key, last.Value), unlockable.Location, unlockable.LocationUnique, unlockable.ID, false));

        }

        private static void Received_BundleDiscovered(ModMessageReceivedEventArgs e)
        {
            var data = e.ReadAs<BundleDiscoveredTransferModel>();
            ShopObject.setDiscovered(data.Id, data.Location, data.Value);
        }

        private static void Received_RequestBundleRefresh(ModMessageReceivedEventArgs e)
        {
            var data = e.ReadAs<List<KeyValuePair<string, string>>>();
            var bundles = ShopObject.getAll();

            var transfer = new List<ShopObjectTransferModel>();
            foreach (var bundle in bundles) {
                if (bundle.WasDiscovered && !data.Contains(new KeyValuePair<string, string>(bundle.Unlockable.ID, bundle.Unlockable.LocationUnique)))
                    transfer.Add(new ShopObjectTransferModel(bundle));
            }

            Helper.Multiplayer.SendMessage(transfer, "SendBundleRefresh", modIDs: new[] { ModManifest.UniqueID }, playerIDs: new[] { e.FromPlayerID });
        }

        private static void Received_SendBundleRefresh(ModMessageReceivedEventArgs e)
        {
            var data = e.ReadAs<List<ShopObjectTransferModel>>();

            if (Game1.activeClickableMenu is BundleOverviewMenu menu)
                menu.AppendMissingBundles(data);

            else if (Game1.activeClickableMenu is StardewValley.Menus.GameMenu or StardewValley.Menus.ItemGrabMenu)
                DonatableItemTooltip.AppendMissingBundles(data);

        }

        private static void Received_UpdateMailData(ModMessageReceivedEventArgs e)
        {
            AssetRequested.MailData = e.ReadAs<Dictionary<string, string>>();

            //Translating the mail if possible
            var unlockables = Helper.GameContent.Load<Dictionary<string, UnlockableModel>>("UnlockableBundles/Bundles");
            foreach (var unlockable in unlockables) {
                var mailKey = Unlockable.GetMailKey(unlockable.Key);

                if (AssetRequested.MailData.ContainsKey(mailKey))
                    AssetRequested.MailData[mailKey] = unlockable.Value.BundleCompletedMail;
            }

            Helper.GameContent.InvalidateCache("Data/Mail");

        }

        private static void Received_ReapplyLocation(ModMessageReceivedEventArgs e)
        {
            if (!Context.IsWorldReady)
                return;

            var location = e.ReadAs<string>();

            var needToBeApplied = MapPatches.AppliedUnlockables.Where(el => el.LocationUnique == location);

            foreach (var unlockable in needToBeApplied)
                MapPatches.ApplyUnlockable(new Unlockable(unlockable), false);

        }

        private static void Received_DebugWarpToHost(ModMessageReceivedEventArgs e)
        {
            var master = Game1.MasterPlayer;
            var masterLocation = master.currentLocation;
            Game1.warpFarmer(masterLocation.Name, master.TilePoint.X + 1, master.TilePoint.Y, 2);

            Game1.player.previousLocationName = Game1.player.currentLocation.Name;
            Game1.currentLocation = Game1.getLocationFromName(masterLocation.NameOrUniqueName, masterLocation.isStructure.Value);
            Game1.currentLocation.reloadMap();
            Game1.locationRequest = new LocationRequest(masterLocation.NameOrUniqueName, masterLocation.isStructure.Value, Game1.currentLocation);
            Game1.xLocationAfterWarp = master.TilePoint.X;
            Game1.yLocationAfterWarp = master.TilePoint.Y;
            Game1.player.position.Value = new Microsoft.Xna.Framework.Vector2(master.Position.X, master.Position.Y);
            Helper.Reflection.GetField<bool>(typeof(Game1), "_isWarping").SetValue(true);

        }

        private static void Received_SPRUpdated(ModMessageReceivedEventArgs e)
        {
            var data = e.ReadAs<KeyValuePair<PlacementRequirementType, string>>();

            switch (data.Key) {
                case PlacementRequirementType.TriggerAction:
                    ModData.Instance.SPRTriggerActionKeys.Add(data.Value);
                    break;
            }

            PlacementRequirement.CheckShopPlacement(data.Key);
        }

        private static void Received_ResetDigSpot(ModMessageReceivedEventArgs e)
        {
            var data = e.ReadAs<DigSpotTransferData>();

            var location = Game1.getLocationFromName(data.Location);
            if (data.Who != 0) {
                var farmer = Game1.getFarmer(data.Who);
                DigSpot.resetDigspot(farmer, location, data.X, data.Y, data.ResetMailFlag);

            } else foreach (var farmer in Game1.getAllFarmers())
                    DigSpot.resetDigspot(farmer, location, data.X, data.Y, data.ResetMailFlag);

        }

        private static void Received_WalletCurrencyChanged(ModMessageReceivedEventArgs e)
        {
            var data = e.ReadAs<CurrencyTransferModel>();
            var currency = WalletCurrencyHandler.GetCurrencyById(data.CurrencyId);

            WalletCurrencyHandler.AddWalletCurrency(currency, data.Who, data.AddedValue, false, currency.Shared);
        }

        private static void Received_BundleFavoriteUpdated(ModMessageReceivedEventArgs e)
        {
            var data = e.ReadAs<BundleFavoriteUpdatedTransferModel>();
            var saveData = ModData.GetUnlockableSaveData(data.BundleId, data.BundleLocation);

            if (data.Favorite)
                saveData.OverviewFavorite.Add(data.Farmer);
            else
                saveData.OverviewFavorite.Remove(data.Farmer);
        }
    }
}
