﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib.AdvancedPricing
{
    public class AdvancedPricing
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> ContextTags { get; set; }
        public string ItemCopy { get; set; }
        public string ItemSprite { get; set; }
        public int ItemSpriteSize { get; set; }
        public string ItemSpriteAnimation { get; set; }
        public string SubItemCopy { get; set; }
        public string SubItemSprite { get; set; }
        public int SubItemSpriteSize { get; set; }
        public string SubItemSpriteAnimation { get; set; }
        public List<string> ItemTypes { get; set; }
    }
}
