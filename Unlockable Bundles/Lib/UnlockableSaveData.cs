﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib
{
    //The savedata for each individual bundle
    public class UnlockableSaveData
    {
        public bool Purchased = false;
        public bool Discovered = false;
        public int DayPurchased = -1;
        public Dictionary<string, int> Price = new();
        public Dictionary<string, int> AlreadyPaid = new();
        public Dictionary<string, int> AlreadyPaidIndex = new();
        public HashSet<long> OverviewFavorite = new();
    }
}
