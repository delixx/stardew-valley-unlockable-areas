﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewValley;
using StardewValley.Locations;
using StardewModdingAPI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Text.RegularExpressions;
using StardewModdingAPI.Events;
using StardewValley.Objects;
using Unlockable_Bundles.Lib.Enums;
using Unlockable_Bundles.API;
using static Unlockable_Bundles.ModEntry;
using StardewModdingAPI.Utilities;

namespace Unlockable_Bundles.Lib
{
    //Makes sure bundle shops are placed every DayStart and LocationListChanged
    //DayEnding shop removal happens in SaveDataEvents
    //SpecialPlacmentRequirements are handled in PlacementRequirement
    public class ShopPlacement
    {
        public static List<GameLocation> ModifiedLocations = new List<GameLocation>();

        //This flag is supposed to prevent locationListChanged from possibly running before dayStarted
        public static bool HasDayStarted = false;

        public static PerScreen<List<UnlockableModel>> UnappliedMapPatches = new(() => new List<UnlockableModel>());

        public static List<Unlockable> BundlesWaitingForTrigger = new();
        public static List<Unlockable> PagesWaitingForBook = new();
        public static List<ShopObject> BooksWaitingForPages = new();

        public static void Initialize()
        {
            Helper.Events.GameLoop.DayStarted += DayStarted;
            Helper.Events.World.LocationListChanged += LocationListChanged;

            Helper.Events.GameLoop.DayEnding += delegate { CleanupDay(); };
            Helper.Events.GameLoop.ReturnedToTitle += ReturnedToTitle; ;
        }

        private static void ReturnedToTitle(object sender, ReturnedToTitleEventArgs e)
        {
            CleanupDay();

            //A splitscreen player leaving through the options menu triggers ReturnedToTitle :3
            if (Context.ScreenId == 0)
                ModData.Instance = null;
        }

        public static void CleanupDay()
        {
            Monitor.Log("cleanupDay was called by " + Multiplayer.GetDebugName(), LogLevel.Debug);
            Multiplayer.IsScreenReady.Value = false;

            UnappliedMapPatches.Value.Clear();
            if (Context.ScreenId > 0)
                return;

            UnlockableBundlesAPI.IsReady = false;
            HasDayStarted = false;
            ModifiedLocations.Clear();
            BundlesWaitingForTrigger.Clear();
            PagesWaitingForBook.Clear();
            BooksWaitingForPages.Clear();

            MapPatches.ClearCache();
            UnlockableBundlesAPI.ClearCache();
        }

        private static void LocationListChanged(object sender, LocationListChangedEventArgs e)
        {
            //This is relevant for buildings
            if (!Context.IsWorldReady || !HasDayStarted || !Context.IsMainPlayer)
                return;

            var unlockables = Helper.GameContent.Load<Dictionary<string, UnlockableModel>>("UnlockableBundles/Bundles");

            foreach (var loc in e.Added.Where(e => !ModifiedLocations.Contains(e)))
                foreach (var model in unlockables.Where(el => LocationMatchesName(el.Value.Location, loc))) {
                    model.Value.ID = model.Key;
                    model.Value.LocationUnique = loc.NameOrUniqueName;

                    if (ModData.IsUnlockablePurchased(model.Key, loc.NameOrUniqueName))
                        continue;

                    model.Value.ApplyDefaultValues();
                    var unlockable = new Unlockable(model.Value);
                    ModData.ApplySaveData(unlockable, model.Value);

                    if (unlockable.UpdatePlacementRequirements())
                        PlaceShop(unlockable, loc);

                    else
                        BundlesWaitingForTrigger.Add(unlockable);

                }
        }

        //resetDay is also called when clients receive the UnlockablesReady message
        public static void ResetDay()
        {
            Monitor.Log("resetDay was called by " + Multiplayer.GetDebugName(), DebugLogLevel);
            HasDayStarted = true;
            Unlockable.CachedJsonAssetIDs.Clear();
            Helper.GameContent.InvalidateCache(asset => asset.NameWithoutLocale.IsEquivalentTo("UnlockableBundles/Bundles"));

            UnlockableBundlesAPI.ClearCache();
            MapPatches.ClearCache();
#if Debug
            BundleDictionary.ClearCache(); //TODO
#endif

            UnlockableBundlesAPI.IsReady = false;
        }


        [EventPriority(EventPriority.Low)] //EventPriority needs to be low, or CP will be very inconsistent with token evaluations before UB dayStarted
        public static void DayStarted(object sender, DayStartedEventArgs e)
        {
            Monitor.Log("Processing ShopPlacement.DayStarted for: " + Multiplayer.GetDebugName(), DebugLogLevel);

            if (!Context.IsMainPlayer) {
                foreach (var unlockable in UnappliedMapPatches.Value)
                    MapPatches.ApplyUnlockable(new Unlockable(unlockable), !Context.IsOnHostComputer);

                UnappliedMapPatches.Value.Clear();
                return;
            }

            ResetDay();
            BundlesWaitingForTrigger = new();
            SaveDataEvents.LoadModData();
            var unlockables = Helper.GameContent.Load<Dictionary<string, UnlockableModel>>("UnlockableBundles/Bundles");
            ModifiedLocations = new List<GameLocation>();
            List<UnlockableModel> applyList = new List<UnlockableModel>();
            AssetRequested.MailData = new Dictionary<string, string>();
#if Debug
            BundleDictionary.Clear();
#endif

            foreach (KeyValuePair<string, UnlockableModel> model in unlockables) {
                model.Value.ID = model.Key;
                model.Value.ApplyDefaultValues();
                ValidateUnlockable(model.Value);

                var locations = GetLocationsFromName(model.Value.Location);
                foreach (var location in locations) {
                    var unlockable = new Unlockable(model.Value);
                    unlockable.LocationUnique = location.NameOrUniqueName;
                    ModData.ApplySaveData(unlockable, model.Value);
#if Debug
                    BundleDictionary.Add(unlockable);
#endif
                    if (ModData.IsUnlockablePurchased(model.Key, location.NameOrUniqueName)) {
                        unlockable._completed.Value = true;
                        applyList.Add((UnlockableModel)unlockable);
                        MapPatches.ApplyUnlockable(unlockable);

                        if (unlockable.IsPage())
                            PlaceShop(unlockable, location);

                    } else if (!unlockable.UpdatePlacementRequirements())
                        BundlesWaitingForTrigger.Add(unlockable);

                    else
                        PlaceShop(unlockable, location);

                    if (model.Value.BundleCompletedMail != "")
                        AssetRequested.MailData.Add(Unlockable.GetMailKey(model.Key), model.Value.BundleCompletedMail);

                }
            }

            Helper.Multiplayer.SendMessage(new KeyValuePair<List<UnlockableModel>, ModData>(applyList, ModData.Instance), "UnlockablesReady", modIDs: new[] { ModManifest.UniqueID });
            Helper.Multiplayer.SendMessage(AssetRequested.MailData, "UpdateMailData", modIDs: new[] { ModManifest.UniqueID });
            Helper.GameContent.InvalidateCache("Data/Mail"); //I kind of wish I could just append my mail data instead of reloading the entire asset
            ModAPI.raiseIsReady(new IsReadyEventArgs(Game1.player));
        }

        public static void PlaceShopsAfterBundleReset(UnlockableModel model)
        {
            model.ApplyDefaultValues();
            var locations = GetLocationsFromName(model.Location);
            foreach (var location in locations) {
                model.LocationUnique = location.NameOrUniqueName;
                var unlockable = new Unlockable(model);
                ModData.ApplySaveData(unlockable, model);

                PlaceShop(unlockable, location);
            }
        }

        private static void ValidateUnlockable(UnlockableModel u)
        {
            if (u.ShopTexture.ToLower() != "none")
                try {
                    Helper.GameContent.Load<Texture2D>(u.ShopTexture);
                } catch (Exception e) { Monitor.LogOnce($"Invalid ShopTexture for '{u.ID}':\n{e}", LogLevel.Error); }

            ValidateAnimation(u.ShopAnimation, nameof(u.ShopAnimation), u.ID);
            ValidateAnimation(u.OverviewAnimation, nameof(u.OverviewAnimation), u.ID);

            ValidatePrice(u, u.Price);
            ValidatePrice(u, u.BundleReward);

            if (u.EditMap.ToLower() != "none")
                try {
                    Helper.GameContent.Load<xTile.Map>(u.EditMap);
                } catch (Exception e) { Monitor.LogOnce($"Invalid EditMap for '{u.ID}':\n{e}", LogLevel.Error); };
        }

        private static void ValidateAnimation(string animation, string name, string id)
        {
            var animationRegex = new Regex("^\\s*(?:(?:\\d+|\\d+\\s*-\\s*\\d+)(?:\\s*@\\s*\\d+){0,1})\\s*(?:,\\s*(?:(?:\\d+|\\d+\\s*-\\s*\\d+)(?:\\s*@\\s*\\d+){0,1})\\s*)*$");
            if (!string.IsNullOrWhiteSpace(animation) && !animationRegex.IsMatch(animation))
                Monitor.LogOnce($"Invalid {name} format for '{id}'. Expected comma seperated \"<From>-<To>@<Interval>\". eg. \"0-2@300, 3, 5-4@600\"", LogLevel.Error);
        }

        private static void ValidatePrice(UnlockableModel u, Dictionary<string, int> price)
        {
            foreach (var el in price) {
                if (el.Key.ToLower().Trim() == "money")
                    continue;

                var first = true;
                foreach (var entry in el.Key.Split(",")) {
                    var id = Unlockable.GetIDFromReqSplit(entry);
                    var quality = Unlockable.GetQualityFromReqSplit(entry);

                    if (quality == -1)
                        Monitor.LogOnce($"Invalid quality '{entry.Split(":").Last()}' for item '{u.ID}': '{id}'", LogLevel.Error);

                    //We suppress this message for subsequent entries as they're considered optional, eg for mod compatibility
                    if (Unlockable.ParseItem(id, el.Value).Name == "Error Item")
                        Monitor.LogOnce($"Invalid item ID for '{u.ID}': '{id}'", first ? LogLevel.Error : LogLevel.Trace);
                    first = false;
                }

            }
        }

        public static void PlaceShop(Unlockable unlockable, GameLocation location)
        {
            if (unlockable.IsPage() && !unlockable.IsBook) {
                var book = BooksWaitingForPages.Find(e => e.Unlockable.ID == unlockable.BookId && e.Unlockable.LocationUnique == unlockable.LocationUnique);
                if (book is null) {
                    PagesWaitingForBook.Add(unlockable);
                    Monitor.Log($"Page waiting '{unlockable.ID}' for book '{unlockable.BookId}' at '{location.NameOrUniqueName}'", DebugLogLevel);

                } else {
                    book._pages.Add(unlockable);
                    Monitor.Log($"Page addedd '{unlockable.ID}' directly to book '{unlockable.BookId}' at '{location.NameOrUniqueName}'", DebugLogLevel);

                }

                return;
            }

            foreach (var tile in unlockable.PossibleShopPositions) {
                if (location.objects.TryGetValue(tile, out var obj)) {
                    if (obj.Category != -999) //We replace litter objects
                        continue;

                    location.removeObject(tile, false);
                }
                if (!ModifiedLocations.Contains(location))
                    ModifiedLocations.Add(location);

                var shopObject = new ShopObject(tile, unlockable);

                if (unlockable.IsBook) {
                    //Add all relevant pages to shopobject._pages and remove them from the waiting list 
                    PagesWaitingForBook.RemoveAll(e => {
                        if (e.BookId == unlockable.ID && e.LocationUnique == unlockable.LocationUnique) {
                            shopObject._pages.Add(e);
                            return true;
                        }
                        return false;
                    });
                    BooksWaitingForPages.Add(shopObject);

                    Monitor.Log($"Book added {shopObject._pages.Count} pages to {unlockable.ID} at {location.NameOrUniqueName} from waiting list", DebugLogLevel);
                }

                location.setObject(tile, shopObject);
                Monitor.Log($"Placed Bundle for '{unlockable.ID}' at '{location.NameOrUniqueName}': {tile}", DebugLogLevel);
                return;
            }

            Monitor.Log($"Failed to place Bundle for '{unlockable.ID}' at '{location.NameOrUniqueName}': {unlockable.ShopPosition} as it is occupied", LogLevel.Warn);
        }

        public static bool ShopExists(Unlockable unlockable) => ShopExists(unlockable, out _);
        public static bool ShopExists(Unlockable unlockable, out Vector2 outTile)
        {
            var location = unlockable.GetGameLocation();
            foreach (var tile in unlockable.PossibleShopPositions) {
                outTile = tile;

                if (location.objects.TryGetValue(tile, out var obj) && obj is ShopObject shop && shop.Unlockable.ID == unlockable.ID)
                    return true;
            }
            outTile = default(Vector2);
            return false;
        }

        public static void RemoveShop(Unlockable unlockable)
        {
            if (ShopExists(unlockable, out var tile)) {
                var location = unlockable.GetGameLocation();
                location.removeObject(tile, false);
                Monitor.Log($"Removed Shop Object for '{unlockable.ID}' at '{location.NameOrUniqueName}':'{tile}'");
            }

        }

        //Returns a list of all GameLocations with the same Name.
        //In the case of buildings they'll share the Name but have a different NameOrUniqueName
        public static List<GameLocation> GetLocationsFromName(string name)
        {
            var res = new List<GameLocation>();

            Utility.ForEachLocation((loc) => {
                if (LocationMatchesName(name, loc))
                    res.Add(loc);

                return true;
            }, true, false);

            return res;
        }

        public static bool LocationMatchesName(string name, GameLocation loc)
        {
            return (loc.Name == name)
                || (loc is Cellar && name == "Cellar") //Cellar location names are enumerated
                || (loc is FarmHouse && name == "FarmHouse"); //Farmhand FarmHouses are called "Cabin"
        }
    }
}
