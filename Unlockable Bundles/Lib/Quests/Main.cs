﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib.Quests
{
    internal class Main
    {
        public static void Initialize()
        {
            CustomQuestHandler.Initialize();
            GrowGiantCropsQuest.Initialize();
        }
    }
}
