﻿using HarmonyLib;
using Microsoft.Xna.Framework;
using Netcode;
using StardewValley;
using StardewValley.Quests;
using StardewValley.TerrainFeatures;
using System.Xml.Serialization;
using static Unlockable_Bundles.ModEntry;

namespace Unlockable_Bundles.Lib.Quests
{
    public class GrowGiantCropsQuest
    {
        public const int QuestTypeId = 888566; //XUB
        public const string QuestType = "UB_GrowGiantCrops";

        public static void Initialize()
        {
            var harmony = new Harmony(ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.DeclaredConstructor(typeof(GiantCrop), new[] { typeof(string), typeof(Vector2) }),
                postfix: new HarmonyMethod(typeof(GrowGiantCropsQuest), nameof(GrowGiantCropsQuest.GiantCrop_Constructor_Postfix))
            );

            harmony.Patch(
                original: AccessTools.DeclaredMethod(typeof(ItemHarvestQuest), nameof(ItemHarvestQuest.OnItemReceived),
                    new[] { typeof(Item), typeof(int), typeof(bool) }),
                prefix: new HarmonyMethod(typeof(GrowGiantCropsQuest), nameof(GrowGiantCropsQuest.OnItemReceived_Prefix))
            );
        }

        public static bool OnItemReceived_Prefix(ItemHarvestQuest __instance, Item item, int numberAdded, bool probe = false)
            => __instance.questType.Value != QuestTypeId; //Skip when it's our custom quest


        public static bool OnGiantCropWasGrown(IQuest quest, string cropId)
        {
            if (quest is not ItemHarvestQuest itemHarvestQuest || itemHarvestQuest.questType.Value != QuestTypeId)
                return false;

            if (itemHarvestQuest.completed.Value || cropId != itemHarvestQuest.ItemId.Value)
                return false;

            var newRequiredAmount = itemHarvestQuest.Number.Value - 1;
            bool complete = newRequiredAmount <= 0;

            itemHarvestQuest.Number.Value = newRequiredAmount;
            if (complete)
                itemHarvestQuest.questComplete();

            return true;
        }

        //This constructor is only called when a new GiantCrop is created
        public static void GiantCrop_Constructor_Postfix(string id)
        {
            Game1.player.NotifyQuests((quest => OnGiantCropWasGrown(quest, id)));
        }

        public static Quest ParseQuest(string[] fields, string id)
        {
            var logConditionsParseError = Helper.Reflection.GetMethod(typeof(Quest), "LogConditionsParseError");

            if (!ArgUtility.TryGet(fields, 4, out var rawConditions, out var error, false)) {
                return logConditionsParseError.Invoke<Quest>(id, error);
            }
            fields = ArgUtility.SplitBySpace(rawConditions);

            if (!ArgUtility.TryGet(fields, 0, out var giantCropId, out error, allowBlank: false) || !ArgUtility.TryGetOptionalInt(fields, 1, out var requiredAmount, out error, 1)) {
                return logConditionsParseError.Invoke<Quest>(id, error);
            }

            var q = new ItemHarvestQuest(giantCropId, requiredAmount);
            q.questType.Value = QuestTypeId;
            return q;
        }
    }
}
