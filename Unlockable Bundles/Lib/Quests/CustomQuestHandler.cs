﻿using HarmonyLib;
using StardewValley;
using StardewValley.Quests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Unlockable_Bundles.ModEntry;

namespace Unlockable_Bundles.Lib.Quests
{
    internal class CustomQuestHandler
    {
        public static void Initialize()
        {
            var harmony = new Harmony(ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.Method(typeof(Quest), nameof(Quest.getQuestFromId), new[] { typeof(string) }),
                prefix: new HarmonyMethod(typeof(CustomQuestHandler), nameof(CustomQuestHandler.getQuestFromId_Prefix))
            );
        }

        //Very hardcoded, because transpilers wouldn't want to work no matter how simple
        public static bool getQuestFromId_Prefix(ref Quest __result, string id)
        {
            string[] fields = Quest.GetRawQuestFields(id);

            if (fields == null)
                return true;

            if (!ArgUtility.TryGet(fields, 0, out var questType, out var error, allowBlank: false) || !ArgUtility.TryGet(fields, 1, out var title, out error, allowBlank: false) || !ArgUtility.TryGet(fields, 2, out var description, out error, allowBlank: false) || !ArgUtility.TryGetOptional(fields, 3, out var objective, out error, null, allowBlank: false) || !ArgUtility.TryGetOptional(fields, 5, out var rawNextQuests, out error, null, allowBlank: false) || !ArgUtility.TryGetInt(fields, 6, out var moneyReward, out error) || !ArgUtility.TryGetOptional(fields, 7, out var rewardDescription, out error, null, allowBlank: false) || !ArgUtility.TryGetOptionalBool(fields, 8, out var canBeCancelled, out error))
                return true;

            Quest q;
            if (questType == GrowGiantCropsQuest.QuestType)
                q = GrowGiantCropsQuest.ParseQuest(fields, id);
            else
                return true;

            if (q is null)
                return false;

            string[] nextQuests = ArgUtility.SplitBySpace(rawNextQuests);

            q.id.Value = id;
            q.questTitle = title;
            q.questDescription = description;
            q.currentObjective = objective;
            string[] array = nextQuests;
            for (int i = 0; i < array.Length; i++) {
                string nextQuest = array[i];
                if (nextQuest.StartsWith('h')) {
                    if (!Game1.IsMasterGame) {
                        continue;
                    }
                    nextQuest = nextQuest.Substring(1);
                }
                q.nextQuests.Add(nextQuest);
            }
            q.showNew.Value = true;
            q.moneyReward.Value = moneyReward;
            q.rewardDescription.Value = ((moneyReward == -1) ? null : rewardDescription);
            q.canBeCancelled.Value = canBeCancelled;

            __result = q;
            return false;
        }
    }
}
