﻿using Netcode;
using StardewValley.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.NetLib;
using Unlockable_Bundles.Lib;
using static Unlockable_Bundles.ModEntry;

//Concept for storing all bundle data in a shared dictionary instead of on the shopobjects.
//Has advantage in multiplayer of not needing to fetch all missing bundles every time the player opens the inventory for the tooltips and overview menu
//I am currently unsure of how to implement the static NetObjectList. Might need a NetRoot, but that seems very troublesome to implement
namespace Unlockable_Bundles.Lib
{
#if Debug
    internal class BundleDictionary
    {
        private static NetObjectList<Unlockable> _bundles = new();

        private static Dictionary<string, Unlockable> _bundlesLookupTable = new();

        public static void Initialize()
        {
            //_bundles.
        }

        public static void Set(List<Unlockable> bundles)
        {
            _bundles.Clear();
            _bundlesLookupTable.Clear();

            foreach (var bundle in bundles)
                _bundles.Add(bundle);
        }

        public static void Add(Unlockable bundle)
        {
            _bundles.Add(bundle);
        }

        public static Unlockable Get(string uniqueLocationKey, string id)
        {
            var lookupKey = $"{uniqueLocationKey}::{id}";

            if (_bundlesLookupTable.TryGetValue(lookupKey, out var result))
                return result;

            result = _bundles.FirstOrDefault(e => e.LocationUnique == uniqueLocationKey && e.ID == id);

            if (result is null) {
                Monitor.Log($"{Multiplayer.GetDebugName()} tried accessing unkown bundle: {lookupKey})", StardewModdingAPI.LogLevel.Error);
                return null;
            }

            _bundlesLookupTable[lookupKey] = result;
            return result;
        }

        public static void Clear()
        {
            _bundles.Clear();
            _bundlesLookupTable.Clear();
        }

        public static void ClearCache()
        {
            _bundlesLookupTable.Clear();
        }
    }
#endif
}
