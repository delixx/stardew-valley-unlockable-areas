﻿using HarmonyLib;
using Newtonsoft.Json;
using StardewModdingAPI;
using StardewValley;
using StardewValley.BellsAndWhistles;
using StardewValley.GameData.Shops;
using StardewValley.Internal;
using StardewValley.Menus;
using StardewValley.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unlockable_Bundles.API;
using Unlockable_Bundles.Lib.AdvancedPricing;
using Unlockable_Bundles.Lib.Enums;
using Unlockable_Bundles.Lib.ShopTypes;
using static StardewValley.BellsAndWhistles.ParrotUpgradePerch;
using static Unlockable_Bundles.ModEntry;

namespace Unlockable_Bundles.Lib
{
    internal class ConsoleCommands
    {
        public static void Initialize()
        {
            Helper.ConsoleCommands.Add("ub", "Dev Commands", Commands);
        }

        //Please note that these commands are mainly used for development purposes.
        //There's no guarentee they wont be changed or removed unannounced (apart from those already documented)
        private static void Commands(string command, string[] args)
        {
            if (args.Length == 0) {
                PrintHelp();
                return;
            }

            switch (args[0].ToLower()) {
                case "help":
                    PrintHelp(); break;

                case "ok" or "purchase":
                    DebugPurchase(); break;

                case "event":
                    if (args.Length == 1)
                        Monitor.Log("Missing Bundle Key", LogLevel.Error);
                    else
                        PlayEventScript(args[1]);
                    break;

                case "location":
                    PrintLocationData(); break;

                case "mailkey" or "mail":
                    if (args.Length == 1)
                        Monitor.Log("Missing Bundle Key", LogLevel.Error);
                    else
                        PrintMailKey(args.Skip(1).Join(null, " "));
                    break;

                case "discover":
                    if (args.Length == 1)
                        Monitor.Log("Missing Bundle Key", LogLevel.Error);
                    else
                        DiscoverBundles(args.Skip(1).Join(null, " "), true);
                    break;

                case "undiscover" or "forget":
                    if (args.Length == 1)
                        Monitor.Log("Missing Bundle Key", LogLevel.Error);
                    else
                        DiscoverBundles(args.Skip(1).Join(null, " "), false);
                    break;

                case "quality":
                    Game1.player.CurrentItem.Quality = int.Parse(args[1]); break;

                case "contexttags" or "tags":
                    PrintContextTags(); break;

                case "id":
                    PrintId(); break;

                case "item" or "i":
                    AddItem(args); break;

                case "showdebugnames":
                    Unlockable.ShowDebugNames = !Unlockable.ShowDebugNames; break;

                case "debug":
                    Debug(); break;

                case "apitest":
                    ApiTest(); break;

                case "eventtest":
                    EventTest(); break;

                case "triggeraction" or "action":
                    TriggerAction(args); break;

                case "bed":
                    Game1.currentLocation.furniture.Add(new StardewValley.Objects.BedFurniture("2048", Game1.player.Tile)); break;

                case "warptohost":
                    Helper.Multiplayer.SendMessage("", "DebugWarpToHost", modIDs: new[] { ModManifest.UniqueID }); break;

                case "resetsprkey" or "resetspr":
                    if (args.Length == 1)
                        Monitor.Log("Missing Key", LogLevel.Error);
                    else
                        ResetSPRKey(args[1]);
                    break;

                case "reset" or "resetbundle":
                    if (args.Length == 1)
                        Monitor.Log("Missing Key", LogLevel.Error);
                    else
                        ResetBundle(args[1]);
                    break;

                case "warp":
                    if (args.Length == 1)
                        Monitor.Log("Missing Bundle Key", LogLevel.Error);
                    else
                        WarpToBundle(args[1]);
                    break;

                case "savedata":
                    PrintSaveData(); break;

                case "showperitem":
                    if (args.Length == 1)
                        ShowPerItem(new List<string>());
                    else
                        ShowPerItem(args.Skip(1));
                    break;

                case "hasflag":
                    if (args.Length == 1)
                        Monitor.Log("Missing flag argument", LogLevel.Error);
                    else
                        PrintHasFlag(args[1]);
                    break;


                default:
                    Monitor.Log("Unknown Command: " + args[0], LogLevel.Error); break;
            }
        }

        private static void PrintHasFlag(string flag)
        {
            Monitor.Log(Game1.player.hasOrWillReceiveMail(flag).ToString(), LogLevel.Info);
        }

        private static void ShowPerItem(IEnumerable<string> conditions)
        {
            var ret = new List<Item>();
            ItemQueryContext itemQueryContext = new();

            var perItemCondition = conditions.Join(null, " ");

            if(perItemCondition.ToLower().Contains("{{modid}}")) {
                Monitor.Log("Please note that this command cannot evaluate tokens like {{ModID}}", LogLevel.Warn);
                return;
            }

            var items = ItemQueryResolver.TryResolve("ALL_ITEMS (O)", itemQueryContext, perItemCondition: perItemCondition);

            ShopMenu shop = new ShopMenu("DebugItemQuery", new Dictionary<ISalable, ItemStockInformation>());
            ItemQueryResult[] array = items;
            foreach (ItemQueryResult entry in array) {
                shop.AddForSale(entry.Item, new ItemStockInformation(0, int.MaxValue));
            }
            Game1.activeClickableMenu = shop;
        }

        private static void PrintSaveData()
        {
            if (!Context.IsWorldReady) {
                Monitor.Log("No savefile loaded", LogLevel.Error);
                return;
            }

            Monitor.Log("//** UnlockableSaveData (Bundle Data)", LogLevel.Warn);
            Monitor.Log(JsonConvert.SerializeObject(ModData.Instance.UnlockableSaveData, Formatting.Indented) + "\n", LogLevel.Info);

            Monitor.Log("//** SPRTriggerActionKeys (SPR Data)", LogLevel.Warn);
            Monitor.Log(JsonConvert.SerializeObject(ModData.Instance.SPRTriggerActionKeys, Formatting.Indented) + "\n", LogLevel.Info);

            Monitor.Log("//** WalletCurrencyData", LogLevel.Warn);
            Monitor.Log(JsonConvert.SerializeObject(ModData.Instance.WalletCurrencyData, Formatting.Indented) + "\n", LogLevel.Info);

            Monitor.Log("//** WalletCurrencyTotal", LogLevel.Warn);
            Monitor.Log(JsonConvert.SerializeObject(ModData.Instance.WalletCurrencyTotal, Formatting.Indented) + "\n", LogLevel.Info);

            Monitor.Log("//** PrizeMachineData", LogLevel.Warn);
            Monitor.Log(JsonConvert.SerializeObject(ModData.Instance.PrizeMachineData, Formatting.Indented) + "\n", LogLevel.Info);

            Monitor.Log("//** FoundUniqueDigSpots", LogLevel.Warn);
            Monitor.Log(JsonConvert.SerializeObject(ModData.Instance.FoundUniqueDigSpots, Formatting.Indented) + "\n", LogLevel.Info);
        }

        private static void WarpToBundle(string key)
        {
            if (!Context.IsWorldReady) {
                Monitor.Log("No savefile loaded", LogLevel.Error);
                return;
            }

            var bundles = ShopObject.getAll();
            var bundleKeys = bundles.Select(shop => shop.Unlockable.ID).ToList();
            var bundleId = Utility.fuzzySearch(key, bundleKeys);

            ShopObject bundle;
            if (bundleId is null) {
                bundleKeys = bundles.Select(shop => shop.Unlockable.GetDisplayName()).ToList();
                bundleId = Utility.fuzzySearch(key, bundleKeys);

                if (bundleId is null) {
                    Monitor.Log($"No bundle found under {key}", LogLevel.Error);
                    return;
                } else
                    bundle = bundles.Find(shop => shop.Unlockable.GetDisplayName() == bundleId);

            } else
                bundle = bundles.Find(shop => shop.Unlockable.ID == bundleId);

            var warp = new Warp(Game1.player.TilePoint.X, Game1.player.TilePoint.Y, bundle.Location.NameOrUniqueName, (int)bundle.TileLocation.X, (int)bundle.TileLocation.Y, false);
            Monitor.Log($"Warping {Multiplayer.GetDebugName()} to bundle {bundle.Unlockable.ID} at {warp.TargetName} : {bundle.TileLocation}", LogLevel.Info);
            Game1.player.warpFarmer(warp);
        }

        private static void ResetBundle(string key)
        {
            if (!Context.IsWorldReady) {
                Monitor.Log("No savefile loaded", LogLevel.Error);
                return;
            }

            var shops = ShopObject.getAll();
            var relevantShops = shops.Where(el => el.Unlockable.ID == key || el._pages.Any(page => page.ID == key));
            var unlockables = Helper.GameContent.Load<Dictionary<string, UnlockableModel>>("UnlockableBundles/Bundles");

            if (ModData.Instance.UnlockableSaveData.Remove(key, out var keyLocationPair) || relevantShops.Count() > 0) {
                foreach (var shop in relevantShops)
                    if (shop.Unlockable.ID == key) {
                        ShopPlacement.RemoveShop(shop.Unlockable);
                        ShopPlacement.PagesWaitingForBook.AddRange(shop._pages);
                    } else
                        shop._pages.RemoveWhere(page => page.ID == key);

                if (unlockables.TryGetValue(key, out var model))
                    ShopPlacement.PlaceShopsAfterBundleReset(model);

                if (Game1.activeClickableMenu is DialogueBox or DialogueShopMenu or BundlePageMenu or BundleBookMenu)
                    Game1.activeClickableMenu = null;

                Monitor.Log($"Successfully reset {keyLocationPair?.Count ?? 0} savedata entries and {relevantShops.Count()} bundle objects/pages with key {key}", LogLevel.Info);
            } else {
                var bundleKeys = shops.Select(shop => shop.Unlockable.ID).ToList();
                var bundleId = Utility.fuzzySearch(key, bundleKeys);

                if (bundleId is null) {
                    bundleKeys = ModData.Instance.UnlockableSaveData.Select(el => el.Key).ToList();
                    bundleId = Utility.fuzzySearch(key, bundleKeys);
                }

                Monitor.Log($"No bundle object, bundle page or bundle savedata found under {key}. {(bundleId is null ? "" : $"Did you mean {bundleId}")}", LogLevel.Error);
            }
        }

        private static void ResetSPRKey(string key)
        {
            if (ModData.Instance.SPRTriggerActionKeys.Remove(key))
                Monitor.Log("Key: " + key + " removed from memory", LogLevel.Info);
            else
                Monitor.Log($"No such key '{key}' in memory", LogLevel.Info);
        }

        private static void TriggerAction(string[] args)
        {
            var action = args.Skip(1).Join(null, " ");

            if (!TriggerActionManager.TryRunAction(action, out string error, out Exception exception))
                Monitor.Log(error, LogLevel.Error);
        }

        private static void PrintValidCommands() =>
            Monitor.Log("Valid Commands: help, purchase, event, location, mailkey, discover, undiscover, showdebugnames, quality, tags, item, triggeraction", LogLevel.Info);

        private static void PrintHelp() =>
            Monitor.Log(
                "Valid Commands:\n" +
                "PURCHASE           Completes the bundle whose menu is currently open or the closest.\n" +
                "EVENT KEY          Fires the ShopEvent script of the Bundle. Must be in the intended location\n" +
                "LOCATION           Prints the current location\n" +
                "MAILKEY KEY        Prints the resulting mail key of a bundle key\n" +
                "DISCOVER KEY       Discovers all bundle Shops matching KEY. Use key ALL to discover all bundles.\n" +
                "UNDISCOVER KEY     Forgets all bundle shops matching KEY. Use key ALL to forget all bundles.\n" +
                "SHOWDEBUGNAMES     Toggles bundle keys & bundle names in the bundle overview menu\n" +
                "QUALITY 0-4        Sets the quality of the currently held item\n" +
                "CONTEXTTAGS        Prints all context tags of the currently held item. Alt. TAGS\n" +
                "ITEM ID [AMOUNT]   Adds the specified item to your inventory. Accepts UB specific Syntax\n" +
                "ACTION ARGS        Executes the specified Triggeraction ARGS\n" +
                "RESETSPR KEY       Resets the specified key for the TriggerAction SPR\n" +
                "RESETBUNDLE KEY    Resets all savedata of the specified bundle and replaces the shops. Alt. RESET\n" +
                "WARP KEY           Warps to the specified bundle using a fuzzy search\n" +
                "SAVEDATA           Prints all ub savedata\n" +
                "SHOWPERITEM [COND] Opens a shop with all objects that match the item query conditions\n" +
                "HASFLAG KEY        Checks the mail flag state for the current player"
                    , LogLevel.Info);

        private static void AddItem(string[] args)
        {
            if (args.Length == 1) {
                Monitor.Log("No itemID specified", LogLevel.Error);
                return;
            }

            var id = Unlockable.GetIDFromReqSplit(args[1]);
            var quality = Unlockable.GetQualityFromReqSplit(args[1]);
            var stack = args.Length > 2 ? int.Parse(args[2]) : 1;

            var item = Unlockable.ParseItem(id, stack, quality);

            if (item is AdvancedPricingItem apItem) {
                if (apItem.UsesFlavoredSyntax) {
                    apItem.ItemCopy.Quality = quality;
                    apItem.ItemCopy.Stack = stack;
                    item = apItem.ItemCopy;
                } else {
                    Monitor.Log($"The ITEM command does not accept advanced pricing syntax apart from auto generated flavored Items!", LogLevel.Error);
                    return;
                }
            }

            Game1.player.addItemToInventory(item);
        }

        private static void PrintId()
        {
            if (!Context.IsWorldReady) {
                Monitor.Log("No savefile loaded", LogLevel.Error);
                return;
            }

            var item = Game1.player.CurrentItem;

            if (item is null) {
                Monitor.Log("No Item selected", LogLevel.Error);
                return;
            }

            Monitor.Log(item.QualifiedItemId, LogLevel.Info);
        }

        private static void PrintContextTags()
        {
            if (!Context.IsWorldReady) {
                Monitor.Log("No savefile loaded", LogLevel.Error);
                return;
            }

            var item = Game1.player.CurrentItem;

            if (item is null) {
                Monitor.Log("No Item selected", LogLevel.Error);
                return;
            }

            var res = $"All context tags for item '{item.Name}' {item.QualifiedItemId}:";
            var tags = item.GetContextTags();
            foreach (var tag in tags)
                res += "\n" + tag;

            Monitor.Log(res, LogLevel.Info);
        }

        private static void PrintLocationData()
        {
            if (!Context.IsWorldReady) {
                Monitor.Log("No savefile loaded", LogLevel.Error);
                return;
            }

            var loc = Game1.currentLocation;
            Monitor.Log($"\nLocation: {loc.Name}\n"
                + $"Unique: {loc.NameOrUniqueName}\n"
                + $"Map: {loc.Map.assetPath}", LogLevel.Info);
        }

        private static void PrintMailKey(string key)
            => Monitor.Log(Unlockable.GetMailKey(key), LogLevel.Info);

        private static void DiscoverBundles(string key, bool wasdiscovered)
        {
            if (!Context.IsWorldReady) {
                Monitor.Log("No savefile loaded", LogLevel.Error);
                return;
            }

            var shops = ShopObject.getAll();

            if (key.EndsWith('*'))
                shops = shops.Where(el => el.Unlockable.ID.ToLower().StartsWith(key.Remove(key.Length - 1).ToLower())).ToList();
            else if (key.ToLower().Trim() != "all")
                shops = shops.Where(el => el.Unlockable.ID.ToLower() == key.ToLower()).ToList();

            shops.ForEach(el => el.WasDiscovered = wasdiscovered);

            Monitor.Log("Done!", LogLevel.Info);
        }

        private static void Debug()
        {
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();
        }

        private static void PlayEventScript(string key)
        {
            if (!Context.IsWorldReady) {
                Monitor.Log("No savefile loaded", LogLevel.Error);
                return;
            }

            var unlockables = Helper.GameContent.Load<Dictionary<string, UnlockableModel>>("UnlockableBundles/Bundles");
            Game1.globalFadeToBlack(() => Game1.currentLocation.startEvent(new UBEvent(new Unlockable(unlockables[key]), unlockables[key].ShopEvent, Game1.player)));
        }

        private static void DebugPurchase()
        {
            if (!Context.IsWorldReady) {
                Monitor.Log("No savefile loaded", LogLevel.Error);
                return;
            }

            if (Game1.activeClickableMenu is DialogueShopMenu menu) {
                menu.Unlockable.ProcessPurchase();
                menu.ScreenSwipe = new ScreenSwipe(0);
                menu.CompletionTimer = 800;
                menu.Complete = true;
                menu.CanClick = false;

            } else if (Game1.activeClickableMenu is BundlePageMenu ccMenu) {
                ccMenu.Unlockable.ProcessPurchase();
                ccMenu.ScreenSwipe = new ScreenSwipe(0);
                ccMenu.CompletionTimer = 800;
                ccMenu.Complete = true;
                ccMenu.CanClick = false;
            } else {
                var tile = Game1.player.Tile;
                var loc = Game1.player.currentLocation;
                StardewValley.Object obj;

                obj = loc.getObjectAtTile((int)tile.X, (int)tile.Y);
                if (obj is not ShopObject)
                    obj = loc.getObjectAtTile((int)tile.X, (int)tile.Y - 1);
                if (obj is not ShopObject)
                    obj = loc.getObjectAtTile((int)tile.X + 1, (int)tile.Y);
                if (obj is not ShopObject)
                    obj = loc.getObjectAtTile((int)tile.X, (int)tile.Y + 1);
                if (obj is not ShopObject)
                    obj = loc.getObjectAtTile((int)tile.X - 1, (int)tile.Y);

                if (obj is ShopObject shop) {
                    switch (shop.ShopType) {
                        case ShopType.CCBundle or ShopType.Dialogue:
                            Monitor.Log("This bundle type requires its menu to be open to debug purchase", LogLevel.Warn); break;
                        case ShopType.ParrotPerch or ShopType.SpeechBubble:
                            Game1.activeClickableMenu = null;
                            shop.SpeechBubble.CurrentState.Value = UpgradeState.StartBuilding;
                            shop.Unlockable.ProcessPurchase();
                            shop.SpeechBubble.WaitingForProcessShopEvent = true;
                            break;

                    }
                } else
                    Monitor.Log("No Bundle Shop in direct vicinity", LogLevel.Info);
            }
        }

        private static void ApiTest()
        {
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();

            IUnlockableBundlesAPI api = Helper.ModRegistry.GetApi<IUnlockableBundlesAPI>(ModManifest.UniqueID);

            var bundles = api.getBundles();
            var purchased = api.PurchasedBundles;
            var purchasedSince = api.PurchaseBundlesByLocation;
        }

        private static void EventTest()
        {
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();

            IUnlockableBundlesAPI api = Helper.ModRegistry.GetApi<IUnlockableBundlesAPI>(ModManifest.UniqueID);
            api.BundlePurchasedEvent += EventTestMethod;
        }

        private static void EventTestMethod(object source, IBundlePurchasedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
                System.Diagnostics.Debugger.Break();
        }
    }
}
