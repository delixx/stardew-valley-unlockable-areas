﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib
{
    public class AnimatedTexture
    {
        public Texture2D Texture;
        private string Animation;
        private long LastUpdatedTick; //Supposed to prevent animations being updated multiple times for the same tick in splitscreen and BundleOverviewMenu
        private int Frame = 0;
        private long Timer = 0;
        private List<KeyValuePair<int, int>> Sequence = new List<KeyValuePair<int, int>>();  //ImageIndex, Tempo
        private Rectangle SourceRectangle;
        private int MaxLoops;
        private int CurrentLoop;

        public Vector2 Position; //Helps with ui animations

        //TODO: Replace every use case of this constructor with a source rectangle
        public AnimatedTexture(Texture2D texture, string animation, int width, int height, int maxLoops = 0) : this(texture, animation, new Rectangle(0, 0, width, height), maxLoops)
        { }
        public AnimatedTexture(Texture2D texture, string animation, Rectangle rectangle, int maxLoops = 0)
        {
            Texture = texture;
            Animation = animation;
            SourceRectangle = rectangle;
            ResetFrames();

            MaxLoops = maxLoops;
        }

        public Rectangle GetOffsetRectangle() => GetOffsetRectangle(ref Frame, Sequence, Texture, SourceRectangle);
        private static Rectangle GetOffsetRectangle(ref int frame, List<KeyValuePair<int, int>> sequence, Texture2D texture, Rectangle sourceRectangle)
        {
            var offsetRectangle = new Rectangle(sourceRectangle.X, sourceRectangle.Y, sourceRectangle.Width, sourceRectangle.Height);

            if (sequence.Count > 0 && (texture.Width / sourceRectangle.Width) > 1)
                offsetRectangle.X = sourceRectangle.Width * sequence.ElementAt(frame).Key;

            return offsetRectangle;
        }

        public void ResetFrames() => ResetFrames(Animation, ref Frame, Sequence);
        private static void ResetFrames(string animation, ref int frame, List<KeyValuePair<int, int>> sequence)
        {
            if (animation is null || animation.Trim() == "")
                return;

            frame = 0;
            sequence.Clear();

            var currentTempo = 100;
            foreach (var entry in animation.Split(",")) {
                var tempoSplit = entry.Split("@");

                if (tempoSplit.Count() > 1)
                    currentTempo = int.Parse(tempoSplit.Last());

                var framesSplit = tempoSplit.First().Split("-");

                var from = int.Parse(framesSplit.First());
                var to = int.Parse(framesSplit.Last());

                if (from < to)
                    for (int i = from; i <= to; i++)
                        sequence.Add(new KeyValuePair<int, int>(i, currentTempo));
                else
                    for (int i = from; i >= to; i--)
                        sequence.Add(new KeyValuePair<int, int>(i, currentTempo));

            }
        }

        public bool Update(GameTime time) => Update(time, ref LastUpdatedTick, ref Timer, ref Frame, Sequence, ref MaxLoops, ref CurrentLoop);
        private static bool Update(GameTime time, ref long lastUpdatedTick, ref long timer, ref int frame, List<KeyValuePair<int, int>> sequence, ref int maxLoops, ref int currentLoop)
        {
            if (time.TotalGameTime.Ticks == lastUpdatedTick)
                return true;

            lastUpdatedTick = time.TotalGameTime.Ticks;

            if (sequence.Count == 0)
                return true;

            if (timer > 0)
                timer -= time.ElapsedGameTime.Milliseconds;

            if (timer <= 0) {
                frame++;
                if (frame >= sequence.Count) {
                    frame = 0;
                    if (maxLoops > 0)
                        currentLoop++;
                }


                timer = sequence.ElementAt(frame).Value;
            }

            return currentLoop < maxLoops;
        }
    }
}
