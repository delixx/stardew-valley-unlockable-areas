﻿using HarmonyLib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using StardewModdingAPI;
using StardewValley;
using StardewValley.BellsAndWhistles;
using StardewValley.Buildings;
using StardewValley.Menus;
using System;
using System.Collections.Generic;
using System.Linq;
using Unlockable_Bundles.Lib.Enums;
using Unlockable_Bundles.Lib.ShopTypes;
using Unlockable_Bundles.NetLib;
using static StardewValley.LocalizedContentManager;
using static Unlockable_Bundles.ModEntry;


namespace Unlockable_Bundles.Lib
{
    public class BundleOverviewMenu : IClickableMenu
    {
        private static Texture2D BGTexture;

        public static void Initialize()
        {
            BGTexture = Helper.GameContent.Load<Texture2D>("UnlockableBundles/UI/BundleOverviewBG");
        }

        private Rectangle ScrollBarRunner;

        public ClickableTextureComponent UpArrow;
        public ClickableTextureComponent DownArrow;
        public ClickableTextureComponent ScrollBar;

        public const int ListElementsPerPage = 4;
        public int DescriptionRowsPerPage = 3;
        public const int RequirementsPerPage = 10;
        private bool Scrolling;

        public List<ClickableComponent> ListElement = new List<ClickableComponent>();
        private List<ClickableTextureComponent> FavoriteButtons = new();
        private List<ShopObject> BaseBundles = new();
        private List<ShopObject> Bundles = new();
        public ShopObject CurrentBundle = null;
        private ShopObject CurrentBook = null;


        public int CurrentItemIndex;
        private int PreviousItemIndex;

        protected int DescriptionScrollIndex = 0;
        protected string[] SplitPreviewDescription = new string[] { };
        public ClickableTextureComponent DescriptionUpArrow;
        public ClickableTextureComponent DescriptionDownArrow;

        protected int RequirementPageIndex = 0;
        public List<ClickableRequirementTexture> RequirementIcons = new();
        private ClickableRequirementTexture HoveredRequirement = null;
        public ClickableTextureComponent RequirementUpArrow;
        public ClickableTextureComponent RequirementDownArrow;

        private Item NextItem = null;
        private string NextId = "";

        private Texture2D OpenBookTexture;
        private Texture2D ClosedBookTexture;

        private bool ClickableComponentsDirty = false;

        public BundleOverviewMenu() : base(Game1.uiViewport.Width / 2 - 320, Game1.uiViewport.Height - 64 - 192, 640, 192)
        {
            OpenBookTexture = Helper.GameContent.Load<Texture2D>("UnlockableBundles/UI/OverviewBookOpen");
            ClosedBookTexture = Helper.GameContent.Load<Texture2D>("UnlockableBundles/UI/OverviewBookClosed");

            updatePosition();
            initializeUpperRightCloseButton();

            CreateClickTableTextures();
            applyTab();
            CustomPopulateClickableComponentList();
            calculateDescriptionRowsPerPage();
        }

        public void calculateDescriptionRowsPerPage()
        {
            DescriptionRowsPerPage = Helper.GameContent.CurrentLocaleConstant switch {
                LanguageCode.de => 4,
                LanguageCode.en => 3,
                LanguageCode.es => 3,
                LanguageCode.fr => 3,
                LanguageCode.hu => 3,
                LanguageCode.it => 3,
                LanguageCode.ja => 4,
                LanguageCode.ko => 2,
                LanguageCode.pt => 3,
                LanguageCode.ru => 4,
                LanguageCode.tr => 3,
                LanguageCode.zh => 4,
                _ => 3
            };
        }

        public void UpdateListElementNeighbors()
        {
            ClickableComponent last_valid_button = ListElement[0];
            for (int i = 0; i < ListElement.Count; i++) {
                ClickableComponent button = ListElement[i];
                button.upNeighborImmutable = true;
                button.downNeighborImmutable = true;
                button.upNeighborID = ((i > 0) ? (i + 3546 - 1) : (-7777));
                button.downNeighborID = ((i < 3 && i < Bundles.Count - 1) ? (i + 3546 + 1) : (-7777));
                if (i >= Bundles.Count) {
                    if (button == currentlySnappedComponent) {
                        currentlySnappedComponent = last_valid_button;
                        if (Game1.options.SnappyMenus)
                            snapCursorToCurrentSnappedComponent();

                    }
                } else {
                    last_valid_button = button;
                }
            }
        }

        private void UpdateRequirementIconNeighbors()
        {
            if (RequirementIcons.Any()) {
                DescriptionUpArrow.rightNeighborID = RequirementIcons.First().myID;
                DescriptionDownArrow.rightNeighborID = RequirementIcons.First().myID;
                RequirementUpArrow.leftNeighborID = RequirementIcons.Last().myID;
                RequirementDownArrow.leftNeighborID = RequirementIcons.Last().myID;

                for (int i = 0; i < RequirementIcons.Count; i++) {
                    var requirement = RequirementIcons[i];
                    var isBottom = RequirementIcons.Any(e => e.bounds.Y < requirement.bounds.Y);
                    var isOnlyRow = !RequirementIcons.Any(e => e.bounds.Y != requirement.bounds.Y);

                    //Check if there's a left neighbor requirement icon
                    if (i != 0 && RequirementIcons[i - 1].bounds.Y == requirement.bounds.Y)
                        requirement.leftNeighborID = RequirementIcons[i - 1].myID;
                    else {
                        if (isBottom) {
                            requirement.leftNeighborID = DescriptionDownArrow.myID;
                            DescriptionDownArrow.rightNeighborID = requirement.myID;
                        } else {
                            requirement.leftNeighborID = DescriptionUpArrow.myID;
                            DescriptionUpArrow.rightNeighborID = requirement.myID;
                        }
                    }

                    //Check if there's a right neighbor requirement icon
                    if (i + 1 < RequirementIcons.Count && RequirementIcons[i + 1].bounds.Y == requirement.bounds.Y)
                        requirement.rightNeighborID = RequirementIcons[i + 1].myID;
                    else {
                        if (isBottom) {
                            requirement.rightNeighborID = RequirementDownArrow.myID;
                            RequirementDownArrow.leftNeighborID = requirement.myID;
                        } else {
                            requirement.rightNeighborID = RequirementUpArrow.myID;
                            RequirementUpArrow.leftNeighborID = requirement.myID;
                        }
                    }
                }

            } else {
                DescriptionUpArrow.rightNeighborID = RequirementUpArrow.myID;
                DescriptionDownArrow.rightNeighborID = RequirementDownArrow.myID;
                RequirementUpArrow.leftNeighborID = DescriptionUpArrow.myID;
                RequirementDownArrow.leftNeighborID = DescriptionDownArrow.myID;
            }
        }

        private void CreateClickTableTextures()
        {
            UpArrow = new ClickableTextureComponent(new Rectangle(xPositionOnScreen + width + 16, yPositionOnScreen + 16, 44, 48), Game1.mouseCursors, new Rectangle(421, 459, 11, 12), 4f) {
                myID = 97865,
                downNeighborID = 106,
                leftNeighborID = 3546
            };
            DownArrow = new ClickableTextureComponent(new Rectangle(xPositionOnScreen + width + 16, yPositionOnScreen + height - 260, 44, 48), Game1.mouseCursors, new Rectangle(421, 472, 11, 12), 4f) {
                myID = 106,
                upNeighborID = 97865,
                downNeighborID = 803,
                leftNeighborID = 3546,
            };
            ScrollBar = new ClickableTextureComponent(new Rectangle(UpArrow.bounds.X + 12, UpArrow.bounds.Y + UpArrow.bounds.Height + 4, 24, 40), Game1.mouseCursors, new Rectangle(435, 463, 6, 10), 4f);
            ScrollBarRunner = new Rectangle(ScrollBar.bounds.X, UpArrow.bounds.Y + UpArrow.bounds.Height + 4, ScrollBar.bounds.Width, DownArrow.bounds.Y - UpArrow.bounds.Y - 64);

            for (int i = 0; i < ListElementsPerPage; i++) {
                ListElement.Add(new ClickableComponent(new Rectangle(xPositionOnScreen + 16, yPositionOnScreen + 16 + i * ((height - 256) / 4), width - 32, (height - 256) / 4 + 4), string.Concat(i)) {
                    myID = i + 3546,
                    rightNeighborID = 97865,
                    fullyImmutable = true
                });

                var favoriteButton = new ClickableTextureComponent(
                    new(xPositionOnScreen + width - 140, yPositionOnScreen + 32 + i * ((height - 256) / 4), 64, 32),
                    Game1.mouseCursors,
                    new Rectangle(310, 392, 16, 16),
                    2f);

                FavoriteButtons.Add(favoriteButton);
            }

            DescriptionUpArrow = new ClickableTextureComponent(new Rectangle(xPositionOnScreen - 24, yPositionOnScreen + height - 200, 44, 48), Game1.mouseCursors, new Rectangle(421, 459, 11, 12), 4f) {
                myID = 801,
                downNeighborID = 802,
                upNeighborID = 3546,
                rightNeighborID = 803
            };
            DescriptionDownArrow = new ClickableTextureComponent(new Rectangle(xPositionOnScreen - 24, yPositionOnScreen + height - 42, 44, 48), Game1.mouseCursors, new Rectangle(421, 472, 11, 12), 4f) {
                myID = 802,
                upNeighborID = 801,
                rightNeighborID = 804
            };

            RequirementUpArrow = new ClickableTextureComponent(new Rectangle(xPositionOnScreen + width + 16, yPositionOnScreen + height - 200, 44, 48), Game1.mouseCursors, new Rectangle(421, 459, 11, 12), 4f) {
                myID = 803,
                downNeighborID = 804,
                upNeighborID = 106,
                leftNeighborID = 801
            };
            RequirementDownArrow = new ClickableTextureComponent(new Rectangle(xPositionOnScreen + width + 16, yPositionOnScreen + height - 42, 44, 48), Game1.mouseCursors, new Rectangle(421, 472, 11, 12), 4f) {
                myID = 804,
                upNeighborID = 803,
                leftNeighborID = 802
            };

            createBundleRequirementTextures();
        }

        private void createBundleRequirementTextures()
        {
            RequirementIcons.Clear();

            if (CurrentBundle is null)
                return;

            var unlockable = CurrentBundle.Unlockable;
            var noDescriptionOffset = unlockable.GetTranslatedOverviewDescription() == "" ? 300 : 0;
            RequirementIcons = BundlePageMenu.createRequirementTextures(xPositionOnScreen + width - 160 - noDescriptionOffset, yPositionOnScreen + height - 100, unlockable, RequirementPageIndex, RequirementsPerPage);
            CustomPopulateClickableComponentList();
        }

        private void setNextItem()
        {
            NextItem = null;
            NextId = "";

            var u = CurrentBundle.Unlockable;
            var last = u._price.Pairs.Last();
            foreach (var req in u._price.Pairs) {
                if (u._alreadyPaid.ContainsKey(req.Key) && !(req.Key == last.Key && NextId == ""))
                    continue;

                NextId = Unlockable.GetFirstIDFromReqKey(req.Key);
                if (NextId == "money")
                    return;

                NextItem = Unlockable.ParseItem(NextId, req.Value);
                return;
            }
        }

        public void applyTab()
        {
            Game1.playSound("shwip");
            BaseBundles = ShopObject.getAll();
            BaseBundles.RemoveAll(el => !el.WasDiscovered);
            SortBundles();

            DescriptionScrollIndex = 0;
            SplitPreviewDescription = new string[] { };

            CurrentItemIndex = 0;
            SetScrollBarToCurrentIndex();
            UpdateListElementNeighbors();
            SelectFirstRelevantBundle();

            if (!Context.IsMainPlayer!) {
                var existing = new List<KeyValuePair<string, string>>();
                foreach (var bundle in Bundles)
                    existing.Add(new(bundle.Unlockable.ID, bundle.Unlockable.LocationUnique));
                Helper.Multiplayer.SendMessage(existing, "RequestBundleRefresh", modIDs: new[] { ModManifest.UniqueID }, playerIDs: new[] { Game1.MasterPlayer.UniqueMultiplayerID });
            }

        }

        private void SelectFirstRelevantBundle()
        {
            if (!Bundles.Any())
                return;

            currentlySnappedComponent = ListElement[0];
            if (Game1.options.SnappyMenus)
                snapCursorToCurrentSnappedComponent();

            if (Bundles.Count == 1 || (isFavoriteEntry(Bundles.First().Unlockable) && !Bundles.First().Unlockable.IsBook))
                selectListElement(Bundles.First(), false);
        }

        public void updatePosition()
        {
            width = 1000 + IClickableMenu.borderWidth * 2;
            height = 600 + IClickableMenu.borderWidth * 2;
            xPositionOnScreen = Game1.uiViewport.Width / 2 - (1000 + IClickableMenu.borderWidth * 2) / 2;
            yPositionOnScreen = Game1.uiViewport.Height / 2 - (600 + IClickableMenu.borderWidth * 2) / 2;
        }

        public override void snapToDefaultClickableComponent()
        {
            return;
        }

        public override void gameWindowSizeChanged(Rectangle oldBounds, Rectangle newBounds)
        {
            updatePosition();
            initializeUpperRightCloseButton();

            ListElement.Clear();
            FavoriteButtons.Clear();
            CreateClickTableTextures();
            SetScrollBarToCurrentIndex();
        }

        private void downArrowPressed()
        {
            DownArrow.scale = DownArrow.baseScale;
            CurrentItemIndex++;
            SetScrollBarToCurrentIndex();
            UpdateListElementNeighbors();
        }

        private void upArrowPressed()
        {
            if (CurrentItemIndex == 0)
                return;

            UpArrow.scale = UpArrow.baseScale;
            CurrentItemIndex--;
            SetScrollBarToCurrentIndex();
            UpdateListElementNeighbors();
        }

        private void SetScrollBarToCurrentIndex()
        {
            if (Bundles.Count > 0) {
                ScrollBar.bounds.Y = (int)((float)(ScrollBarRunner.Height - ScrollBar.bounds.Height) / Math.Max(1, Bundles.Count - 4) * CurrentItemIndex + UpArrow.bounds.Bottom + 4);
                if (CurrentItemIndex == Bundles.Count - 4) {
                    ScrollBar.bounds.Y = DownArrow.bounds.Y - ScrollBar.bounds.Height - 4;
                }
            }
        }

        public void selectListElement(ShopObject bundle, bool sound = true)
        {
            if (sound)
                Game1.playSound("coin");
            if (CurrentBook == bundle) {
                CurrentItemIndex = PreviousItemIndex;
                PreviousItemIndex = 0;
                CurrentBook = null;
                SortBundles();
            } else if (bundle.Unlockable.IsBook)
                CurrentBook = bundle;

            CurrentBundle = bundle;
            DescriptionScrollIndex = 0;
            RequirementPageIndex = 0;
            SplitPreviewDescription = new string[] { };
            createBundleRequirementTextures();

            if (CurrentBundle is null)
                return;

            if (CurrentBook is not null && CurrentBook == CurrentBundle) {
                currentlySnappedComponent = ListElement[0];
                if (Game1.options.SnappyMenus)
                    snapCursorToCurrentSnappedComponent();

                PreviousItemIndex = CurrentItemIndex;
                CurrentItemIndex = 0;
                SortBundles();
            } else if (!CurrentBundle.Unlockable.IsBook)
                setNextItem();
        }

        public override void receiveRightClick(int x, int y, bool playSound = true)
        {
            exitThisMenu();
            base.receiveRightClick(x, y, playSound);
        }

        public override void receiveLeftClick(int x, int y, bool playSound = true)
        {
            base.receiveLeftClick(x, y);

            if (DownArrow.containsPoint(x, y) && IsScrollbarDrawn() && CurrentItemIndex < Math.Max(0, Bundles.Count - 4)) {
                downArrowPressed();
                Game1.playSound("shwip");
            } else if (UpArrow.containsPoint(x, y) && IsScrollbarDrawn()) {
                upArrowPressed();
                Game1.playSound("shwip");
            } else if (ScrollBarRunner.Contains(x, y) && IsScrollbarDrawn()) {
                Scrolling = true;
            } else if (base.upperRightCloseButton.containsPoint(x, y)) {
                exitThisMenu();
                return;
            } else if (DescriptionUpArrow.containsPoint(x, y) && DescriptionScrollIndex != 0) {
                DescriptionScrollIndex--;
                Game1.playSound("shwip");
            } else if (DescriptionDownArrow.containsPoint(x, y) && HasNextDescriptionRow()) {
                DescriptionScrollIndex++;
                Game1.playSound("shwip");
            } else if (RequirementUpArrow.containsPoint(x, y) && RequirementPageIndex != 0) {
                RequirementPageIndex--;
                Game1.playSound("shwip");
                createBundleRequirementTextures();
            } else if (RequirementDownArrow.containsPoint(x, y) && HasNextRequirementPage()) {
                RequirementPageIndex++;
                Game1.playSound("shwip");
                createBundleRequirementTextures();
            }


            for (int i = 0; i < ListElement.Count; i++) {
                if (CurrentItemIndex + i >= Bundles.Count || !ListElement[i].containsPoint(x, y))
                    continue;

                int index = CurrentItemIndex + i;

                if (FavoriteButtons[i].containsPoint(x, y)) {
                    toggleFavorite(Bundles[index].Unlockable);
                    SortBundles();
                    return;
                }

                selectListElement(Bundles[index]);

                UpdateListElementNeighbors();
                SetScrollBarToCurrentIndex();
                return;
            }
        }
        private bool isFavoriteEntry(Unlockable unlockable)
        {
            var saveData = ModData.GetUnlockableSaveData(unlockable.ID, unlockable.LocationUnique);
            return saveData.OverviewFavorite.Contains(Game1.player.UniqueMultiplayerID);
        }
        private bool toggleFavorite(Unlockable unlockable)
        {
            var saveData = ModData.GetUnlockableSaveData(unlockable.ID, unlockable.LocationUnique);
            bool favorite;

            if (saveData.OverviewFavorite.Contains(Game1.player.UniqueMultiplayerID)) {
                saveData.OverviewFavorite.Remove(Game1.player.UniqueMultiplayerID);
                favorite = false;
            } else {
                saveData.OverviewFavorite.Add(Game1.player.UniqueMultiplayerID);
                favorite = true;
            }

            if (!Context.IsMainPlayer) {
                var transferData = new BundleFavoriteUpdatedTransferModel() {
                    BundleId = unlockable.ID,
                    BundleLocation = unlockable.LocationUnique,
                    Farmer = Game1.player.UniqueMultiplayerID,
                    Favorite = favorite
                };
                Helper.Multiplayer.SendMessage(transferData, "BundleFavoriteUpdated", modIDs: new[] { ModManifest.UniqueID }, playerIDs: new[] { Game1.MasterPlayer.UniqueMultiplayerID });
            }

            if (favorite)
                Game1.playSound("dwop");
            else
                Game1.playSound(closeSound);

            return favorite;
        }

        private void SortBundles()
        {
            var baseBundles = BaseBundles;
            if (CurrentBook is not null) {
                baseBundles = new();
                baseBundles.Add(CurrentBook);
                foreach (var page in CurrentBook._pages.Where(e => !e._completed.Value))
                    baseBundles.Add(new ShopObject(page));
            }

            Bundles = baseBundles
                .OrderBy(e => !e.Unlockable.IsBook && CurrentBook is not null)
                .ThenBy(e => !isFavoriteEntry(e.Unlockable))
                .ThenBy(e => e.Unlockable.Location)
                .ThenBy(e => e.Unlockable.GetDisplayName())
                .ToList();
        }


        public override void leftClickHeld(int x, int y)
        {
            if (Scrolling) {
                int y2 = ScrollBar.bounds.Y;
                ScrollBar.bounds.Y = Math.Min(yPositionOnScreen + height - 64 - 12 - ScrollBar.bounds.Height, Math.Max(y, yPositionOnScreen + UpArrow.bounds.Height + 20));
                float percentage = (float)(y - ScrollBarRunner.Y) / (float)ScrollBarRunner.Height;
                CurrentItemIndex = Math.Min(Bundles.Count - 4, Math.Max(0, (int)Math.Round((double)((Bundles.Count - 4) * percentage))));
                SetScrollBarToCurrentIndex();
                UpdateListElementNeighbors();
                if (y2 != ScrollBar.bounds.Y) {
                    Game1.playSound("shiny4");
                }
            }
        }

        public override void releaseLeftClick(int x, int y)
        {
            base.releaseLeftClick(x, y);
            Scrolling = false;
        }

        public override void receiveScrollWheelAction(int direction)
        {
            if (!IsScrollbarDrawn())
                return;

            if (direction > 0 && CurrentItemIndex > 0) {
                upArrowPressed();
                Game1.playSound("shiny4");
            } else if (direction < 0 && CurrentItemIndex < Math.Max(0, Bundles.Count - 4)) {
                downArrowPressed();
                Game1.playSound("shiny4");
            }
        }

        public override void receiveGamePadButton(Buttons b)
        {
            if (b == Buttons.LeftShoulder && DescriptionScrollIndex != 0) {
                DescriptionScrollIndex--;
                Game1.playSound("shwip");
            } else if (b == Buttons.RightShoulder && HasNextDescriptionRow()) {
                DescriptionScrollIndex++;
                Game1.playSound("shwip");
            } else if (b == Buttons.LeftTrigger && RequirementPageIndex != 0) {
                RequirementPageIndex--;
                Game1.playSound("shwip");
                createBundleRequirementTextures();
            } else if (b == Buttons.RightTrigger && HasNextRequirementPage()) {
                RequirementPageIndex++;
                Game1.playSound("shwip");
                createBundleRequirementTextures();
            } else if (b == Buttons.Y) {
                if (CurrentBundle is not null)
                    toggleFavorite(CurrentBundle.Unlockable);
                SortBundles();
            }

            base.receiveGamePadButton(b);
        }

        public override void receiveKeyPress(Keys key)
        {
            if (Game1.options.menuButton.Contains(new InputButton(key)) && !Game1.input.GetGamePadState().IsButtonDown(Buttons.Y))
                exitThisMenu();

            base.receiveKeyPress(key);
        }

        public override void performHoverAction(int x, int y)
        {
            UpArrow.tryHover(x, y, 0.5f);
            DownArrow.tryHover(x, y, 0.5f);
            DescriptionUpArrow.tryHover(x, y, 0.5f);
            DescriptionDownArrow.tryHover(x, y, 0.5f);
            upperRightCloseButton.tryHover(x, y, 0.5f);
            RequirementUpArrow.tryHover(x, y, 0.5f);
            RequirementDownArrow.tryHover(x, y, 0.5f);

            HoveredRequirement = RequirementIcons.FirstOrDefault(c => c.bounds.Contains(x, y));

            foreach (var favoriteButton in FavoriteButtons)
                favoriteButton.tryHover(x, y, 0.25f);
        }

        protected override void customSnapBehavior(int direction, int oldRegion, int oldID)
        {
            if (!IsScrollbarDrawn())
                return;

            switch (direction) {
                case 2: {
                        if (CurrentItemIndex < Math.Max(0, Bundles.Count - 4)) {
                            downArrowPressed();
                            Game1.playSound("shiny4");

                            currentlySnappedComponent = getComponentWithID(0);
                            snapCursorToCurrentSnappedComponent();
                        }
                        break;
                    }
                case 0:
                    if (CurrentItemIndex > 0) {
                        upArrowPressed();
                        Game1.playSound("shiny4");

                        currentlySnappedComponent = getComponentWithID(3546);
                        snapCursorToCurrentSnappedComponent();
                    }
                    break;
            }
        }

        public override bool readyToClose()
        {
            GamePadState currentPadState = Game1.input.GetGamePadState();

            if (((currentPadState.IsButtonDown(Buttons.Start) && !Game1.oldPadState.IsButtonDown(Buttons.Start)) || (currentPadState.IsButtonDown(Buttons.B) && !Game1.oldPadState.IsButtonDown(Buttons.B))))
                exitThisMenu();

            return false;
        }

        public override void draw(SpriteBatch b)
        {
            b.Draw(Game1.staminaRect, new Rectangle(0, 0, Game1.graphics.GraphicsDevice.Viewport.Width, Game1.graphics.GraphicsDevice.Viewport.Height), Color.Black * 0.75f);

            //Top/Bottom Window w Borders
            b.Draw(BGTexture, new Rectangle(xPositionOnScreen - 30, yPositionOnScreen - 30, width + 140, height + 80), Color.White);

            drawListElements(b);
            drawBundleSummary(b);

            if (IsScrollbarDrawn()) {
                drawTextureBox(b, Game1.mouseCursors, new Rectangle(403, 383, 6, 6), ScrollBarRunner.X, ScrollBarRunner.Y, ScrollBarRunner.Width, ScrollBarRunner.Height, Color.White, 4f);
                ScrollBar.draw(b);
                UpArrow.draw(b);
                DownArrow.draw(b);
            }

            if (HoveredRequirement != null && HoveredRequirement.item != null)
                drawToolTip(b, HoveredRequirement.item.getDescription(), HoveredRequirement.item.DisplayName, HoveredRequirement.item);
            else if (HoveredRequirement != null)
                drawHoverText(b, HoveredRequirement.hoverText, Game1.dialogueFont);

            base.draw(b);
            drawMouse(b);
        }

        private void drawListElements(SpriteBatch b)
        {
            for (int k = 0; k < ListElement.Count; k++) {
                if (CurrentItemIndex + k >= Bundles.Count)
                    continue;

                var le = ListElement[k];
                ShopObject shop = Bundles[CurrentItemIndex + k];
                Unlockable unlockable = shop.Unlockable;

                var pageIndent = unlockable.IsPage() ? 16 : 0;
                IClickableMenu.drawTextureBox(b, Game1.mouseCursors, new Rectangle(384, 396, 15, 15), le.bounds.X + pageIndent, le.bounds.Y, le.bounds.Width - pageIndent, le.bounds.Height, (le.containsPoint(Game1.getOldMouseX(), Game1.getOldMouseY()) && !Scrolling) ? Color.Wheat * 0.5f : Color.White * 0.5f, 4f, drawShadow: false);

                //ShopTexture
                shop.drawInMenu(b, new Vector2(le.bounds.X + 24 + pageIndent, le.bounds.Y + 15), 1.3f);

                //display Name
                SpriteText.drawString(b, unlockable.GetDisplayName(), le.bounds.X + 82 + pageIndent, le.bounds.Y + 28, 999999, -1, 999999, 1f, 0.88f, junimoText: false, -1, "");

                //Location + Coordinates
                var loc = unlockable.GetGameLocation();
                var locationDisplay = $"{loc.DisplayName} X{shop.TileLocation.X} Y{shop.TileLocation.Y}";
                if (!unlockable.IsPage())
                    b.DrawString(Game1.smallFont, locationDisplay, new Vector2(le.bounds.X + le.bounds.Width - 14 - Game1.smallFont.MeasureString(locationDisplay).Length(), le.bounds.Y + 60), Color.Gray);

                //Progress
                drawListElementProgress(b, le, shop);

                //FavoriteButton
                var saveData = ModData.GetUnlockableSaveData(unlockable.ID, unlockable.LocationUnique);
                var favoriteButton = FavoriteButtons[k];
                favoriteButton.sourceRect = saveData.OverviewFavorite.Contains(Game1.player.UniqueMultiplayerID) ? new(294, 392, 16, 16) : new(310, 392, 16, 16);
                favoriteButton.draw(b);
                if (Game1.options.gamepadControls && shop == CurrentBundle)
                    b.Draw(Game1.controllerMaps, new Rectangle(favoriteButton.bounds.X + 20, favoriteButton.bounds.Y - 8, 13, 13), Utility.controllerMapSourceRect(new(598, 260, 26, 26)), Color.White);


                if (shop == CurrentBundle)
                    //Red selection rectangle
                    IClickableMenu.drawTextureBox(b, Game1.mouseCursors, new Rectangle(375, 357, 3, 3), le.bounds.X + 4 + pageIndent, le.bounds.Y + 4, le.bounds.Width - 8 - pageIndent, le.bounds.Height - 8, Color.White, 4f, drawShadow: false);

                //BookIcon
                if (unlockable.IsBook)
                    b.Draw(shop == CurrentBook ? OpenBookTexture : ClosedBookTexture, new Vector2(le.bounds.X - 12, le.bounds.Y - 12), null, Color.White, 0f, new(), 3f, SpriteEffects.None, 1f);
            }
        }

        private void drawListElementProgress(SpriteBatch b, ClickableComponent le, ShopObject shop)
        {
            var unlockable = shop.Unlockable;
            int current;
            int max;

            if (unlockable.IsBook) {
                current = shop._pages.Where(e => e._completed.Value).Count();
                max = shop._pages.Count;
            } else {
                current = unlockable._alreadyPaid.Count();
                max = (unlockable.ShopType is ShopType.CCBundle ? unlockable.BundleSlots : unlockable._price.Count());
            }

            var progressDisplay = $"{current}/{max}";
            b.DrawString(Game1.smallFont, progressDisplay, new Vector2(le.bounds.X + le.bounds.Width - 14 - Game1.smallFont.MeasureString(progressDisplay).Length(), le.bounds.Y + 20), Color.Gray);
        }

        private void drawBundleSummary(SpriteBatch b)
        {
            if (Bundles.Count == 0) {
                string emptyMenuDescription = Helper.Translation.Get("ub_empty_overview");
                emptyMenuDescription = Game1.parseText(emptyMenuDescription, Game1.dialogueFont, width - 72);
                b.DrawString(Game1.dialogueFont, emptyMenuDescription, new Vector2(xPositionOnScreen + 42, yPositionOnScreen + height - 200), Game1.textColor);
            }

            if (CurrentBundle is null)
                return;

            var u = CurrentBundle.Unlockable;

            //Title Scroll
            string title = AddSpacesForTitleIcon() + u.GetDisplayName();
            var xPosTitle = xPositionOnScreen + ((width + 60) / 2) - (SpriteText.getWidthOfString(title) / 2);
            SpriteText.drawStringWithScrollBackground(b, title, xPosTitle, yPositionOnScreen + height - 250);
            CurrentBundle.drawInMenu(b, new Vector2(xPosTitle + 12, yPositionOnScreen + height - 247), 0.85f);

            DrawRequirementIcons(b);

            //Location + Coordinates
            var loc = u.GetGameLocation();
            var locationDisplay = u.IsPage() ? "" : $"{loc.DisplayName} X{CurrentBundle.TileLocation.X} Y{CurrentBundle.TileLocation.Y}";
            b.DrawString(Game1.dialogueFont, locationDisplay, new Vector2(xPositionOnScreen + 42, yPositionOnScreen + height - 186), Color.DarkSlateGray);

            //Progress
            drawBundleSummaryProgress(b, CurrentBundle, locationDisplay);

            var moneyString = u.ShopType == ShopType.ParrotPerch ? Helper.Translation.Get("ub_parrot_money") : Helper.Translation.Get("ub_speech_money");
            var description = u.GetTranslatedOverviewDescription();
            if (NextItem is not null)
                description = description.Replace("{{item}}", NextId == "money" ? moneyString : NextItem.DisplayName);

            if (SplitPreviewDescription.Length == 0 && description != "")
                SplitPreviewDescription = Game1.parseText(description, Game1.dialogueFont, 670).Split(Environment.NewLine);

            description = "";
            for (int i = DescriptionScrollIndex; i < DescriptionScrollIndex + DescriptionRowsPerPage; i++) {
                if (SplitPreviewDescription.Length > i)
                    description += SplitPreviewDescription[i] + Environment.NewLine;
            }
            b.DrawString(Game1.dialogueFont, description, new Vector2(xPositionOnScreen + 42, yPositionOnScreen + height - 145), Game1.textColor);

            if (DescriptionScrollIndex != 0)
                DescriptionUpArrow.draw(b);

            if (HasNextDescriptionRow())
                DescriptionDownArrow.draw(b);

            if (RequirementPageIndex != 0)
                RequirementUpArrow.draw(b);

            if (HasNextRequirementPage())
                RequirementDownArrow.draw(b);
        }

        private void drawBundleSummaryProgress(SpriteBatch b, ShopObject shop, string locationDisplay)
        {
            var unlockable = shop.Unlockable;
            int current;
            int max;

            if (unlockable.IsBook) {
                current = shop._pages.Where(e => e._completed.Value).Count();
                max = shop._pages.Count;
            } else {
                current = unlockable._alreadyPaid.Count();
                max = (unlockable.ShopType is ShopType.CCBundle ? unlockable.BundleSlots : unlockable._price.Count());
            }

            var progressDisplay = $"{current}/{max}";
            Vector2 progressPos;

            if (locationDisplay == "")
                progressPos = new Vector2(xPositionOnScreen + 46, yPositionOnScreen + height - 186);
            else if (unlockable.GetTranslatedOverviewDescription() == "")
                progressPos = new Vector2(xPositionOnScreen + 42, yPositionOnScreen + height - 146);
            else if (Game1.dialogueFont.MeasureString(locationDisplay).X < 699 - Game1.dialogueFont.MeasureString(progressDisplay).Length())
                progressPos = new Vector2(xPositionOnScreen + 700 - Game1.smallFont.MeasureString(progressDisplay).Length(), yPositionOnScreen + height - 186);
            else
                progressPos = new Vector2(xPositionOnScreen + 42, yPositionOnScreen + height - 220);


            b.DrawString(Game1.dialogueFont, progressDisplay, progressPos, Color.DarkSlateGray);
        }

        private string AddSpacesForTitleIcon()
        { //I am using spaces to create space for the bundle shoptexture in the scroll, but spaces have different width depending on language
            var s = " ";
            while (SpriteText.getWidthOfString(s) < 52)
                s += " ";

            return s;
        }

        public void DrawRequirementIcons(SpriteBatch b)
        {
            var unlockable = CurrentBundle.Unlockable;

            for (int i = 0; i < RequirementIcons.Count; i++) {
                float alpha_mult = 1f;

                //if (CurrentPartialRequirementIndex >= 0 && CurrentPartialRequirementIndex != i)
                //    alpha_mult = 0.25f;

                ClickableRequirementTexture c = RequirementIcons[i];
                bool completed = unlockable._alreadyPaid.ContainsKey(c.ReqKey);

                if (!completed)
                    b.Draw(Game1.shadowTexture, new Vector2(c.bounds.Center.X - Game1.shadowTexture.Bounds.Width * 4 / 2 - 4, c.bounds.Center.Y + 4), Game1.shadowTexture.Bounds, Color.White * alpha_mult, 0f, Vector2.Zero, 4f, SpriteEffects.None, 0.1f);

                if (c.ReqItemId == "money") {
                    c.draw(b, Color.White * (completed ? 0.25f : alpha_mult), 0.89f);
                    UtilityMisc.DrawKiloFormat(b, c.ReqValue, c.bounds.X, c.bounds.Y, Color.White * (completed ? 0.25f : alpha_mult));

                } else if (c.item != null && c.visible)
                    c.item.drawInMenu(b, new Vector2(c.bounds.X, c.bounds.Y), c.scale / 4f, 1f, 0.9f, StackDrawType.Draw, Color.White * (completed ? 0.25f : alpha_mult), drawShadow: false);
            }
        }

        public bool IsScrollbarDrawn() => Bundles.Count > ListElementsPerPage;
        public bool HasNextDescriptionRow() => SplitPreviewDescription.Length > DescriptionScrollIndex + DescriptionRowsPerPage;
        public bool HasNextRequirementPage() => CurrentBundle?.Unlockable._price.Pairs.Count() > ((RequirementPageIndex + 1) * RequirementsPerPage);

        public void AppendMissingBundles(List<ShopObjectTransferModel> transferModels)
        {
            if (transferModels.Count == 0)
                return;

            foreach (var model in transferModels) {
                var shop = new ShopObject(model);
                BaseBundles.Add(shop);

            }
            SortBundles();

            ListElement.Clear();
            FavoriteButtons.Clear();
            CreateClickTableTextures();
            CustomPopulateClickableComponentList();

            SetScrollBarToCurrentIndex();
            SelectFirstRelevantBundle();
        }

        public void CustomPopulateClickableComponentList()
        {
            populateClickableComponentList();
            allClickableComponents.AddRange(RequirementIcons);

            UpdateListElementNeighbors();
            UpdateRequirementIconNeighbors();
        }

        public override void automaticSnapBehavior(int direction, int oldRegion, int oldID)
        {
            if (ClickableComponentsDirty) {
                ClickableComponentsDirty = false;
                CustomPopulateClickableComponentList();
            }
            base.automaticSnapBehavior(direction, oldRegion, oldID);
        }

        public override void setUpForGamePadMode() => ClickableComponentsDirty = true;
    }
}

