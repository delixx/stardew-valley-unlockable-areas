﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib.WalletCurrency
{
    public class Main
    {
        public static void Initialize()
        {
            WalletCurrencyHandler.Initialize();
            WalletCurrencyBillboard.Initialize();
            _Inventory.Initialize();
            _PowersTab.Initialize();
        }
    }
}
