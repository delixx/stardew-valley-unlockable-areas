﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unlockable_Bundles.Lib.WalletCurrency
{
    public class WalletCurrencyItem
    {
        public string ItemId;
        public int Value = 1;
    }
}
